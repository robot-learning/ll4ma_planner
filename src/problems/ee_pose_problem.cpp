#ifndef SPARSE_GRADIENT
#define SPARSE_GRADIENT true
#endif
#include "ll4ma_planner/problems/ll4ma_opt_problem.hpp"

using namespace opt_problems;
optProblem::optProblem()
{
  std::cerr<<"optProblem constructor initialized"<<std::endl;
}
bool optProblem::updateData(Eigen::VectorXd des_palm_pose,vector<double> vel_limits, vector<int> contact_links, vector<double> preshape,int reach_idx)
{

  arm_dof=7;
  reach_idx_=reach_idx;

  m_dim=arm_dof*timesteps_; // 
  m_nec=0;//

  des_palm_pose_=des_palm_pose;
  
  vel_lim=vel_limits[0];
  cerr<<reach_idx<<"initialized optproblem with dimension: "<<m_dim<<endl;
  
  if(timesteps_>1)
  {
    wa=10.0/timesteps_;
    wm=0.1/timesteps_;
  }
  else
  {
    wa=0.0;
    wm=0.0;
  }
  w_coll=1.0;

  wd=0.0;// reach
  wp=1.0;//timesteps_;// palm pose 
  wp2=0.0*timesteps_;// hand preshape
  wp_world=0.0;// palm orientation is same as world along two axes, good for side grasps
  coll_thresh=0.001; 
  vector<double> or_={1.57,0.0,0.0};
  w_orient=or_;
  contact_links_=contact_links;
  //coll_links_.resize(c_checker.n_links_+c_checker.n_hand_links_-contact_links.size());
  
  for(int i=0;i<c_checker.n_links_+c_checker.n_hand_links_;++i)
  {
      coll_links_.emplace_back(i);
  }
  m_nic=arm_dof*(timesteps_) + timesteps_+1;
  Eigen::Map<Eigen::VectorXd> hand_shape(&preshape[0],preshape.size());
  hand_preshape=hand_shape;
  cerr<<"Opt problem initialized"<<endl;
  c_tol=1e-5;
}

double optProblem::objFunction(const vector<double> x) const
{
  // Cost function is on reaching a desired cartesian pose
  //double wc=10.0;
  // Cost function is on reaching a desired cartesian pose 
  double cost_val=0.0;
  Eigen::VectorXd q;
  vector<double> ee_pose_arr;
  Eigen::VectorXd pos_err,ee_pose,coll_err;
  double orient_err=0.0;
  double contact_cost=0.0;
  Eigen::VectorXd acc_cost(arm_dof);
  
  q.resize(arm_dof);


  vector<double> o_err;

  vector<Eigen::VectorXd> q_arr;
  Eigen::VectorXd j0(arm_dof);
  
  for(int i=0;i<arm_dof;++i)
  {
    j0[i]=joints_0[i];
  }

  
  q_arr.push_back(j0);
  q_arr.push_back(j0);
  
  for(int i=0; i<timesteps_;++i)
  {
    for(int j=0; j<arm_dof;++j)
    {
      q[j]=x[(i)*arm_dof+j];
    }
    q_arr.push_back(q);
  }
  q_arr.push_back(q);
  q_arr.push_back(q);


  // hand preshape match at final timestep:
  //cost_val+=wp2*(hand_preshape-q_arr[2+timesteps_].tail(hand_preshape.size())).squaredNorm();
  // palm pose cost:
  //cost_val+=
  vector<double> state(arm_dof,0.0);

  for(int j=0; j<arm_dof;++j)
  {
    state[j]=x[(timesteps_-1)*arm_dof+j];
  }

  //cost_val+=palmPose(state)[0];
  if(timesteps_<2)
  {
    return cost_val;
  }

  // running costs:
  for(int i=0; i<timesteps_;++i)
  {
    // min acc cost:
    acc_cost=q_arr[i-2+2]-2.0*q_arr[i+2-1]+q_arr[i+2];
    cost_val+=wa*acc_cost.squaredNorm();
    // path length cost
    acc_cost=q_arr[i+2]-q_arr[i-1+2];
    cost_val+=wm*acc_cost.squaredNorm();

  }
  return cost_val;
}
vector<double> optProblem::objGradient(vector<double> x) const
{
  vector<double> retval(m_dim,0.0);

  Eigen::VectorXd pos_err,q,ee_pose;
  Eigen::VectorXd gradient,acc_cost,acc_grad;
  Eigen::MatrixXd J;
  gradient.resize(7);
  gradient.setZero();
  q.resize(arm_dof);

  vector<Eigen::VectorXd> q_arr;
  Eigen::VectorXd j0(arm_dof);
  for(int i=0;i<arm_dof;++i)
  {
    j0[i]=joints_0[i];
  }

  q_arr.push_back(j0);
  q_arr.push_back(j0);
  

  for(int i=0; i<timesteps_;++i)
  {
    for(int j=0; j<arm_dof;++j)
    {
      q[j]=x[(i)*arm_dof+j];
    }
    q_arr.push_back(q);
  }
  q_arr.push_back(q);
  q_arr.push_back(q);

  
  // hand preshape match:
  //  Eigen::VectorXd j_err=(hand_preshape-q_arr[2+timesteps_].tail(hand_preshape.size()));
  //for(int i=0;i<j_err.size();++i)
  {
    //retval[(arm_dof*(timesteps_-1))+7+i]+=-2.0*wp2*j_err[i];
  }
  
  // palm pose grad
    
  vector<double> state(arm_dof,0.0);
  for(int j=0; j<arm_dof;++j)
  {
    state[j]=x[(timesteps_-1)*arm_dof+j];
  }
  /*
  vector<double> gradient_pp=gradPalmPose(state);
  
  for(int i=0;i<gradient_pp.size();++i)
  {
    retval[(arm_dof)*(timesteps_-1)+i]+=gradient_pp[i];
  }
  */
  if(timesteps_==1)
  {
    return retval;
  }
  // acc gradient
  for(int i=0;i<timesteps_;++i)
  {
    q=q_arr[i+2];
    gradient.setZero();
    // min acc grad:
    acc_cost=wa*(q_arr[i-2+2]-2.0*q_arr[i+2-1]+q_arr[i+2]);
    
    
    acc_grad=2.0*acc_cost;
    for (int j=0;j<arm_dof;++j)
    {
      retval[j+arm_dof*(i)]+=acc_grad[j];
    }

    if(i>1)
    {
      acc_grad=2.0*acc_cost;
      for (int j=0;j<arm_dof;++j)
      {
        retval[j+arm_dof*(i-2)]+=acc_grad[j];
      } 
    }
    if(i>0)
    {
      acc_grad=-4.0*acc_cost;
      for (int j=0;j<arm_dof;++j)
      {
        retval[j+arm_dof*(i-1)]+=acc_grad[j];
      }
     
    }
    acc_cost=2.0*wm*(q_arr[i+2]-q_arr[i-1+2]);
    acc_grad=acc_cost;
    
    for (int j=0;j<arm_dof;++j)
    {
      retval[j+arm_dof*(i)]+=acc_grad[j];
    }
    if(i>0)
    {
      acc_grad=-1.0*acc_cost;
      for (int j=0;j<arm_dof;++j)
      {
        retval[j+arm_dof*(i-1)]+=acc_grad[j];
      } 
    }

    
  }
  
  return retval;
}

vector<double> optProblem::inEqConstraints(vector<double> x) const
{
  vector<double> vel_const(m_nic,0.0);
  // Encoding initial joint angle constraint as velocity constraint
  for(int j=0;j<arm_dof;++j)
  {
    double vel=x[(0)*arm_dof+j]-joints_0[j];
    vel_const[(0)*arm_dof+j]=pow((max(abs(vel),vel_lim)-vel_lim),2);
  }

  for (int i=1; i<timesteps_; ++i)
  {
    for(int j=0;j<arm_dof;++j)
    {
      double vel=x[(i)*arm_dof+j]-x[(i-1)*arm_dof+j];
      vel_const[(i)*arm_dof+j]=pow((max(abs(vel),vel_lim)-vel_lim),2);

    }
  }
  // collision constraint:

  vector<double> state(arm_dof,0.0);
  // do not check collisions for contact_links
  for(int i=0;i<timesteps_;++i)
  {
    // collision checker variables:
    double collision_err=0.0;
    
    for(int j=0; j<arm_dof;++j)
    {
      state[j]=x[(i)*arm_dof+j];
    }
    

    vel_const[arm_dof*timesteps_+i]=collisionDistance(state)[0];
    
  }

  // reach palm pose :
  
  for(int j=0; j<arm_dof;++j)
  {
    state[j]=x[(timesteps_-1)*arm_dof+j];
  }

  vel_const[arm_dof*timesteps_+timesteps_]=palmPose(state)[0];
  return vel_const;
}
vector<double> optProblem::inEqGradient(vector<double> x) const
{
  Eigen::MatrixXd J;
    
  
  vector<double> retval(1*arm_dof+2*(arm_dof)*(timesteps_-1)+(arm_dof*1),0.0);
  
  for(int j=0;j<arm_dof;++j)
  {
    double vel=x[j]-joints_0[j];
    double delta=2*(max(abs(vel),vel_lim)-vel_lim);
    retval[j]=1.0*copysign(delta,vel);
  }
  

  for(int i=1;i<timesteps_;++i)
  {
      for(int j=0;j<arm_dof;++j)
      {
        double vel=x[(i)*arm_dof+j]-x[(i-1)*arm_dof+j];
        double delta=2*(max(abs(vel),vel_lim)-vel_lim);
        retval[1*arm_dof+2*(i-1)*arm_dof+arm_dof+j]=1.0*copysign(delta,vel);
        retval[1*arm_dof+2*(i-1)*arm_dof+j]= -1.0*copysign(delta,vel);
      }
  }

  // reaching palm pose:
  // get state:
  
  vector<double> state(arm_dof,0.0);
  for(int j=0; j<arm_dof;++j)
  {
    state[j]=x[(timesteps_-1)*arm_dof+j];
  }

  vector<double> gradient_=gradPalmPose(state);
  for(int i=0;i<gradient_.size();++i)
  {
    retval[1*arm_dof+2*(arm_dof)*(timesteps_-1)+i]=gradient_[i];
  }
  return retval;
}

vector<double> optProblem::collisionGradient(vector<double> x) const
{
  vector<double> retval(arm_dof*timesteps_,0.0);
  //return retval;
  vector<double> gradient(arm_dof,0.0);
  //vector<double> fd_gradient(arm_dof,0.0);

  vector<double> state(arm_dof,0.0);

  for(int i=0;i<timesteps_;++i)
  {
    // get state:
    for(int j=0; j<arm_dof;++j)
    {
      state[j]=x[(i)*arm_dof+j];
    }

    // get collision gradient
    
    gradient=gradColl(state);
        
    // add to retval 
    for(int m=0;m<arm_dof;++m)
    {
      retval[i*arm_dof+m]=gradient[m];
    }
    

  }

  return retval;
}

vector<double> optProblem::EqConstraints(vector<double> x) const
{
  // no constraint
  vector<double> ret(m_nec,0.0);
  
  return  ret;  
}

vector<double> optProblem::eqGradient(vector<double> x) const
{
  vector<double> retval(m_dim*m_nec,0.0);
  
  return retval;
}

vector<double> optProblem::gradient(const vector<double> x) const
{
  vector<double> grad;
  vector<double> obj_grad=objGradient(x);
  vector<double> eq_grad=eqGradient(x);
  vector<double> ineq_grad=inEqGradient(x);
  
  grad.resize(m_dim+m_dim*m_nec+m_dim*m_nic,0.0);
  for(int i=0;i<m_dim;i++)
  {
    grad[i]=obj_grad[i];

  }

  for(int i=m_dim;i<m_dim+m_dim*m_nec;i++)
  {
    grad[i]=eq_grad[i-m_dim];
  }


  for(int i=m_dim+m_dim*m_nec;i<m_dim+m_dim*m_nec+m_dim*m_nic;i++)
  {
    
    grad[i]=ineq_grad[i-m_dim-m_dim*m_nec];
  }

  return grad;
}
vector<vector<double>> optProblem::bounds() const
{
  vector<vector<double>> bound;
  vector<double> up_bounds;
  vector<double> low_bounds;
  
  // Getting joint angle limits from urdf
  for(int i=0;i<timesteps_;++i)
  {
    up_bounds.insert(up_bounds.end(), robot_kdl_->up_bounds.begin(), robot_kdl_->up_bounds.begin()+7);
    low_bounds.insert(low_bounds.end(), robot_kdl_->low_bounds.begin(), robot_kdl_->low_bounds.begin()+7);
    //up_bounds.insert(up_bounds.end(), robot_kdl_->up_bounds.begin(), robot_kdl_->up_bounds.end());
    //low_bounds.insert(low_bounds.end(), robot_kdl_->low_bounds.begin(), robot_kdl_->low_bounds.end());
  }
  bound.resize(2);
  bound[0]=low_bounds;
  bound[1]=up_bounds;
  return bound;
  
}

#if SPARSE_GRADIENT==true
std::vector<std::pair<vector<double>::size_type, vector<double>::size_type>> optProblem::gradient_sparsity() const
{
  std::vector<std::pair<vector<double>::size_type, vector<double>::size_type>> retval;
  // Objective gradient is dense
  for (int i=0;i<m_dim;++i)
  {
    retval.emplace_back(0,i);
  }
  // inequality constraints:
  for(int i=0;i<arm_dof;++i)
  {
    retval.emplace_back(1+m_nec+i,i);
  }
  
  // Other timesteps:
  for(int i=1;i<timesteps_;++i)
  {
    for(int j=0;j<arm_dof;++j)
    {
      retval.emplace_back(1+m_nec+1*arm_dof+(i-1)*arm_dof+j, (i-1)*arm_dof+j);
      retval.emplace_back(1+m_nec+1*arm_dof+(i-1)*arm_dof+j,(i)*arm_dof+j);
    }
  }
  
  // collision constraint:
  for(int i=0;i<timesteps_;++i)
  {
      for(int j=0;j<arm_dof;++j)
      {
        retval.emplace_back(1+m_nec+arm_dof*timesteps_+i,i*arm_dof+j);
      }
  }
  // reach palm pose constraint:
  for(int j=0;j<arm_dof;++j)
  {
    retval.emplace_back(1+m_nec+arm_dof*timesteps_+timesteps_,(timesteps_-1)*arm_dof+j);
  }
  return retval;
}
vector<double> optProblem::sparse_gradient(const vector<double> x) const
{
  vector<double> grad;
  vector<double> obj_grad=objGradient(x);
  vector<double> ineq_grad=inEqGradient(x);
  vector<double> collision_grad=collisionGradient(x);  

  for(int i=0;i<m_dim;i++)
  {
    grad.push_back(obj_grad[i]);
  }

  for(int j=0;j<arm_dof;++j)
  {
    grad.emplace_back(ineq_grad[j]);
  }
  
  for(int i=1;i<timesteps_;++i)
  {
    for(int j=0;j<arm_dof;++j)
    {
      grad.emplace_back(ineq_grad[1*arm_dof+2*(i-1)*arm_dof+j]);
      grad.emplace_back(ineq_grad[1*arm_dof+2*(i-1)*arm_dof+arm_dof+j]);
    }
  }
  // collision constraint:
  for(int i=0;i<timesteps_;++i)
  {
      for(int j=0;j<arm_dof;++j)
      {        
         grad.emplace_back(collision_grad[i*arm_dof+j]);
      }
  }
  // reach gradient:
  for(int j=0;j<arm_dof;++j)
  {
    //cerr<<ineq_grad[1*arm_dof+2*(arm_dof)*(timesteps_-1)+j]<<" ";
    grad.push_back(ineq_grad[1*arm_dof+2*(arm_dof)*(timesteps_-1)+j]);
  }
  return grad;
}
#endif

vector<double> optProblem::collisionDistance(const vector<double> &state) const
{
  vector<double> coll_distance(1,0.0);
  vector<vector<double>> r_poses;
  vector<coll_data> c_data;
  Eigen::VectorXd q;
  q.resize(robot_kdl_->dof);

  // collision checker variables:
  double collision_err=0.0;
    
  for(int j=0; j<arm_dof;++j)
  {
    q[j]=state[j];
  }
  for(int j=0;j<hand_preshape.size();++j)
  {
    q[j+arm_dof]=hand_preshape[j];
  }
    
  robot_kdl_->getLinkPoses(q,r_poses);
    
  c_checker.update_robot_state(r_poses);
    
  c_checker.rob_env_ccheck(c_data);

  // get maximum collision distance:
  collision_err=0.0;
  for(int j=0;j<coll_links_.size();++j)
  {
    int k=j;
    
    if((c_data[k].distance<coll_thresh) && collision_err<(c_data[k].distance-coll_thresh)*(c_data[k].distance-coll_thresh))
    {
      collision_err=(c_data[k].distance-coll_thresh)*(c_data[k].distance-coll_thresh);
    }
    
  } 
    
  coll_distance[0]=w_coll*collision_err;
  return coll_distance;
    
}
vector<double> optProblem::gradColl(vector<double> x) const
{
  vector<double> retval((arm_dof),0.0);

  //return retval;
  vector<vector<double>> r_poses;
  
  vector<coll_data> c_data;
  Eigen::VectorXd q,gradient_;
  q.resize(robot_kdl_->dof);
  Eigen::MatrixXd link_J;

  for(int j=0; j<arm_dof;++j)
  {
    q[j]=x[j];
  }
  for(int j=0;j<hand_preshape.size();++j)
  {
    q[j+arm_dof]=hand_preshape[j];
  }
    
  robot_kdl_->getLinkPoses(q,r_poses);
  c_checker.update_robot_state(r_poses);
  c_checker.rob_env_ccheck(c_data);
  gradient_.resize(robot_kdl_->dof);
  gradient_.setZero();
  double collision_err=0.0;
  int c_idx=0;
  for(int k=0;k<coll_links_.size();++k)
  {
 
    int j=k;//coll_links_[k];
    if(( c_data[j].distance<coll_thresh) && collision_err<std::abs(c_data[j].distance-coll_thresh))
    {
      collision_err=std::abs(c_data[j].distance-coll_thresh);
      c_idx=j;
    }
  }

  int j=c_idx;

  //int k=coll_links_[j];
  if(c_data[j].distance<coll_thresh)
  {
    robot_kdl_->getLinkPtJacobian(j,q,c_data[j].relp1,link_J);
    link_J=link_J.block(0,0,3,link_J.cols());

    Eigen::Map<Eigen::VectorXd> link_pose(&r_poses[j][0],r_poses[j].size());
    Eigen::VectorXd delta_p=c_data[j].p2-c_data[j].p1;
    delta_p=delta_p/delta_p.lpNorm<2>();
    //compute rotation due to distance:
    Eigen::Vector3d d=-1.0*copysign(1.0,c_data[j].distance)*(c_data[j].distance-coll_thresh)*delta_p;
    
        
    gradient_=2.0*w_coll*d.transpose()*link_J;
    /* Slower 
    Eigen::MatrixXd link_J_inv;
    // TODO:  lambda_sq>0.0 slows down convergence, verify if link_J is always full rank.
    double lambda_sq=0.0;
    Eigen::MatrixXd I(link_J.rows(),link_J.rows());
    I.setIdentity();
    gradient_=2.0*w_coll*(link_J.transpose()*(link_J*link_J.transpose()+lambda_sq*I).inverse())*d;
    */
    //gradient_=2.0*w_coll*link_J_inv*d;
    //cerr<<gradient_.transpose()<<endl;
    //cerr<<(2.0*w_coll*d.transpose()*link_J).transpose()<<endl;
  }
  for(int j=0;j<arm_dof;++j)
  {
    retval[j]=gradient_[j];
  }

  return retval;
}

vector<double> optProblem::gradCollFD(vector<double> x) const
{
  vector<double> retval((arm_dof),0.0);
  
  using std::placeholders::_1;

  std::function<vector<double>(vector<double>)> f = std::bind(&optProblem::collisionDistance,this,_1);

  //  retval=pagmo::estimate_gradient(std::bind(&optProblem::forceBalance,this,_1),x);
  retval=pagmo::estimate_gradient(f,x);
    
  return retval; 
}
vector<double> optProblem::palmPose(const vector<double> &state) const
{
  Eigen::VectorXd q(robot_kdl_->dof);
  q.setZero();
  Eigen::VectorXd palm_pose,pose_err(6);
  //cerr<<"S: ";
  for(int j=0; j<arm_dof;++j)
  {
    //cerr<<state[j]<<" ";
    q[j]=state[j];
  }
  for(int j=0;j<hand_preshape.size();++j)
  {
    q[j+arm_dof]=hand_preshape[j];
  }
    
  //cerr<<endl;
  // Palm orientation cost:
  robot_kdl_->getFK(reach_idx_,q,palm_pose,true);
  pose_err.head(3)=des_palm_pose_.head(3)-palm_pose.head(3);
  vector<double> o_d(des_palm_pose_.data()+3, des_palm_pose_.data()+6);    
  vector<double> o_p(palm_pose.data()+3, palm_pose.data()+6);    

  vector<double> orient_error;
  robot_kdl_->euler_diff(o_p,o_d,orient_error);
  //pose_err.head(3)=0.0*pose_err.head(3);

  pose_err[3]=orient_error[0]*0.1;
  pose_err[4]=orient_error[1]*0.1;
  pose_err[5]=orient_error[2]*0.1;
  vector<double> cost(1,0.0);
  cost[0]=wp*pose_err.squaredNorm();
  return cost;
}
vector<double> optProblem::gradPalmPose(vector<double> x) const
{
  vector<double> retval(arm_dof,0.0);
  Eigen::MatrixXd J;
  Eigen::VectorXd q(robot_kdl_->dof);
  for(int j=0; j<arm_dof;++j)
  {
    q[j]=x[j];
  }
  for(int j=0;j<hand_preshape.size();++j)
  {
    q[j+arm_dof]=hand_preshape[j];
  }
    

  Eigen::VectorXd pose_err(6);

  Eigen::VectorXd palm_pose;
  
  // Palm orientation cost:
  robot_kdl_->getFK(reach_idx_,q,palm_pose,true);
  pose_err.head(3)=des_palm_pose_.head(3)-palm_pose.head(3);
  vector<double> o_d(des_palm_pose_.data()+3, des_palm_pose_.data()+6);    
  vector<double> o_p(palm_pose.data()+3, palm_pose.data()+6);    

  vector<double> orient_error;
  robot_kdl_->euler_diff(o_p,o_d,orient_error);
  pose_err[3]=orient_error[0]*0.1;
  pose_err[4]=orient_error[1]*0.1;
  pose_err[5]=orient_error[2]*0.1;
  //pose_err.head(3)=0.0*pose_err.head(3);
  robot_kdl_->getJacobian(reach_idx_,q.head(arm_dof),J);
  //if(wp*pose_err.squaredNorm())
  {
    pose_err[3]*=0.1;
    pose_err[4]*=0.1;
    pose_err[5]*=0.1;
    /*
    Eigen::MatrixXd J_inv;
    robot_kdl_->getPseudoInverse(J,J_inv);
    double lambda_sq=1e-5;
    Eigen::MatrixXd I(J.rows(),J.rows());
    I.setIdentity();
    cerr<<J.transpose()<<endl;
    cerr<<"inv:"<<endl;
    cerr<<J_inv<<endl;
    //Eigen::VectorXd gradient_=-2.0*wp*(J.transpose()*(J*J.transpose()+lambda_sq*I).inverse())*pose_err;
    Eigen::VectorXd gradient_=-2.0*wp*(J_inv)*pose_err;
    //cerr<<gradient_.transpose()<<endl;
    //cerr<<(-2.0*wp*J.transpose()*pose_err).transpose()<<endl;
    */
    Eigen::VectorXd gradient_=-2.0*wp*J.transpose()*pose_err;
    for(int i=0;i<retval.size();++i)
    {
      retval[i]=gradient_[i];
    }

  }
  return retval;
  
}

vector<double> optProblem::reachDistance(const vector<double> &state) const
{
  vector<double> cost(1,0.0);

  // get link pose
  Eigen::VectorXd q;
  q.resize(arm_dof);

  // collision checker variables:


  double collision_err=0.0;
    
  for(int j=0; j<state.size();++j)
  {
    q[j]=state[j];
  }

  Eigen::VectorXd pose;
  robot_kdl_->getFK(reach_idx_,q,pose,false);
  vector<double> r_pose;
  robot_kdl_->getSTDVector(pose,r_pose);

  // update reachability meshes pose
  c_checker.update_reach_pose(r_pose);
  
  // compute signed distance:
  vector<coll_data> c_data;
  c_checker.reach_obj_ccheck(c_data);
  for(int i=0;i<c_data.size();++i)
  {
    if(c_data[i].distance>0.0)
    {
      cost[0]+=wd*c_data[i].distance*c_data[i].distance;
    }
  }
  
  return cost;
}
vector<double> optProblem::reachIdxDistance(const vector<double> &state) const
{
  vector<double> cost(1,0.0);

  // get link pose
  Eigen::VectorXd q;
  q.resize(arm_dof);

  // collision checker variables:


  double collision_err=0.0;
    
  for(int j=0; j<state.size();++j)
  {
    q[j]=state[j];
  }

  Eigen::VectorXd pose;
  robot_kdl_->getFK(reach_idx_,q,pose,false);
  vector<double> r_pose;
  robot_kdl_->getSTDVector(pose,r_pose);

  // update reachability meshes pose
  c_checker.update_reach_pose(r_pose);
  
  // compute signed distance:
  coll_data c_data;
  c_checker.reach_obj_ccheck(c_data,0);
  
  cost[0]=copysign(wd*c_data.distance*c_data.distance,c_data.distance);
  
  return cost;
}
vector<double> optProblem::gradReachIdxDistanceFD(vector<double> state) const
{
  vector<double> retval((arm_dof),0.0);
  
  using std::placeholders::_1;

  std::function<vector<double>(vector<double>)> f = std::bind(&optProblem::reachIdxDistance,this,_1);

  //  retval=pagmo::estimate_gradient(std::bind(&optProblem::forceBalance,this,_1),x);
  retval=pagmo::estimate_gradient(f,state);
  return retval; 
}

vector<double> optProblem::gradReachDistanceFD(vector<double> state) const
{
  vector<double> retval((arm_dof),0.0);
  
  using std::placeholders::_1;

  std::function<vector<double>(vector<double>)> f = std::bind(&optProblem::reachDistance,this,_1);

  //  retval=pagmo::estimate_gradient(std::bind(&optProblem::forceBalance,this,_1),x);
  retval=pagmo::estimate_gradient(f,state);
  return retval; 
}

vector<double> optProblem::reachMidDistance(const vector<double> &state) const
{
  vector<double> cost(1,0.0);

  // get link pose
  Eigen::VectorXd q;
  q.resize(arm_dof);

  // collision checker variables:


  double collision_err=0.0;
    
  for(int j=0; j<state.size();++j)
  {
    q[j]=state[j];
  }

  Eigen::VectorXd pose;
  robot_kdl_->getFK(reach_idx_,q,pose,false);
  vector<double> r_pose;
  robot_kdl_->getSTDVector(pose,r_pose);

  // update reachability meshes pose
  c_checker.update_reach_pose(r_pose);
  
  // compute signed distance:
  coll_data c_data;
  c_checker.reach_obj_ccheck(c_data,1);
  
  cost[0]=copysign(wd*c_data.distance*c_data.distance,c_data.distance);
  
  return cost;
}
vector<double> optProblem::gradReachMidDistanceFD(vector<double> state) const
{
  vector<double> retval((arm_dof),0.0);
  
  using std::placeholders::_1;

  std::function<vector<double>(vector<double>)> f = std::bind(&optProblem::reachMidDistance,this,_1);

  //  retval=pagmo::estimate_gradient(std::bind(&optProblem::forceBalance,this,_1),x);
  retval=pagmo::estimate_gradient_h(f,state);
  return retval; 
}
vector<double> optProblem::reachRngDistance(const vector<double> &state) const
{
  vector<double> cost(1,0.0);

  // get link pose
  Eigen::VectorXd q;
  q.resize(arm_dof);

  // collision checker variables:


  double collision_err=0.0;
    
  for(int j=0; j<state.size();++j)
  {
    q[j]=state[j];
  }

  Eigen::VectorXd pose;
  robot_kdl_->getFK(reach_idx_,q,pose,false);
  vector<double> r_pose;
  robot_kdl_->getSTDVector(pose,r_pose);

  // update reachability meshes pose
  c_checker.update_reach_pose(r_pose);
  
  // compute signed distance:
  coll_data c_data;
  c_checker.reach_obj_ccheck(c_data,2);
  
  cost[0]=copysign(wd*c_data.distance*c_data.distance,c_data.distance);
  
  return cost;
}
vector<double> optProblem::gradReachRngDistanceFD(vector<double> state) const
{
  vector<double> retval((arm_dof),0.0);
  
  using std::placeholders::_1;

  std::function<vector<double>(vector<double>)> f = std::bind(&optProblem::reachRngDistance,this,_1);

  //  retval=pagmo::estimate_gradient(std::bind(&optProblem::forceBalance,this,_1),x);
  retval=pagmo::estimate_gradient_h(f,state);
  return retval; 
}

vector<double> optProblem::reachThbDistance(const vector<double> &state) const
{
  vector<double> cost(1,0.0);

  // get link pose
  Eigen::VectorXd q;
  q.resize(arm_dof);

  // collision checker variables:


  double collision_err=0.0;
    
  for(int j=0; j<state.size();++j)
  {
    q[j]=state[j];
  }

  Eigen::VectorXd pose;
  robot_kdl_->getFK(reach_idx_,q,pose,false);
  vector<double> r_pose;
  robot_kdl_->getSTDVector(pose,r_pose);

  // update reachability meshes pose
  c_checker.update_reach_pose(r_pose);
  
  // compute signed distance:
  coll_data c_data;
  c_checker.reach_obj_ccheck(c_data,3);
  
  cost[0]=copysign(wd*c_data.distance*c_data.distance,c_data.distance);
  
  return cost;
}
vector<double> optProblem::gradReachThbDistanceFD(vector<double> state) const
{
  vector<double> retval((arm_dof),0.0);
  
  using std::placeholders::_1;

  std::function<vector<double>(vector<double>)> f = std::bind(&optProblem::reachThbDistance,this,_1);

  //  retval=pagmo::estimate_gradient(std::bind(&optProblem::forceBalance,this,_1),x);
  retval=pagmo::estimate_gradient_h(f,state);
  return retval; 
}



