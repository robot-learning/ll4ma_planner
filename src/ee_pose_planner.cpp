#include "ll4ma_planner/problems/ll4ma_opt_problem.hpp"
#include <ll4ma_opt_utils/plan_env_utils.hpp>

#include <ll4ma_opt_wrapper/pagmo_opt/wrapper.hpp>

#include <ros/ros.h>
#include <ros/package.h>

// ROS service
#include "ll4ma_planner/GetEEPlan.h"

// joint trajectory msg:
#include <trajectory_msgs/JointTrajectory.h>

using namespace pagmo;
using namespace ll4ma_opt_utils;
class srvClass : public envUtils
{
public:
  vector<int> contact_links;
  int reach_idx;
  bool read_params()
  {
    bool success = true;
    success &= nh->getParam("contact_link_index", contact_links);
    success &= nh->getParam("ee_frame",reach_idx);
    
    return success;
  }

  bool world_call(ll4ma_opt_utils::UpdateWorld::Request &req,ll4ma_opt_utils::UpdateWorld::Response &res)
  {
    return envUtils::world_call(req,res);
  }

  bool add_object_call(ll4ma_opt_utils::AddObject::Request &req, ll4ma_opt_utils::AddObject::Response &res)
  {
    return envUtils::add_object_call(req,res);
  }
  
  bool plan_call(ll4ma_planner::GetEEPlan::Request &req,ll4ma_planner::GetEEPlan::Response &res)
  {
    int T=20;// TODO: make this read from service call
        
    int arm_dof=req.arm_jstate.position.size();
    int hand_dof=req.hand_jstate.position.size();
    
    vector<double> joints_0;
    joints_0.resize(req.arm_jstate.position.size()+req.hand_jstate.position.size());
    for(int i=0;i<req.arm_jstate.position.size();++i)
    {
      joints_0[i]=req.arm_jstate.position[i];
    }

    for(int i=0;i<req.hand_jstate.position.size();++i)
    {
      joints_0[i+req.arm_jstate.position.size()]=req.hand_jstate.position[i];
    }
    
    vector<double> vel_limits;
    int tim_st=req.t_steps;
    
    if(tim_st>1)
    {
      vel_limits.resize(arm_dof,0.2*float(T/float(tim_st)));// total time is T seconds
    }
    else
    {
      vel_limits.resize(arm_dof,100.0);
    }

    
    // get desired contact points from service request
    // send palm pose:
    Eigen::VectorXd pt(6);
    pt.setZero();
    pt[0]=req.des_ee_pose[0].position.x;
    pt[1]=req.des_ee_pose[0].position.y;
    pt[2]=req.des_ee_pose[0].position.z;
    // convert point to rpy:
    vector <double> rp=robot_kdl_.get_RPY_q(req.des_ee_pose[0].orientation.x, req.des_ee_pose[0].orientation.y,req.des_ee_pose[0].orientation.z, req.des_ee_pose[0].orientation.w);
    pt[3]=rp[0];
    pt[4]=rp[1];
    pt[5]=rp[2];
    

    // get hand preshape:
    vector<double> preshape(req.ee_preshape.position.size());
    for(int i=0;i<req.ee_preshape.position.size();++i)
    {
      preshape[i]=req.ee_preshape.position[i];
    }


    // send palm pose:
    
    // visualize desired contact points as markers:

    // get reach meshes' frame:
    bool success = true;
    if(!success)
    {
      ROS_ERROR("Failed to get ee frame");
    }
    
    // Create a opt problem instance
    //vector<int> t_steps={tim_st,int(tim_st*0.8)};
    int t_steps=tim_st;
    //(t_steps,robot_kdl_,c_check,pt,vel_limits,joints_0,contact_links,preshape,reach_idx);

    opt_problems::optProblem opt_problem;
    opt_problem.updateTimesteps(t_steps);
    opt_problem.updateRobotModel(robot_kdl_);
    opt_problem.updateInitialState(joints_0);
    opt_problem.updateCollisionModel(c_check);
    opt_problem.enableCollisionChecking();
    opt_problem.updateData(pt,vel_limits,contact_links,preshape,reach_idx);

    std::cerr<<"Prob initialized"<<opt_problem.bounds()[0].size()<<std::endl;
    problem prob{con_optimization{opt_problem}};

    std::cerr<<"Setting tolerance"<<std::endl;
    vector<double> tol(opt_problem.m_nic,1e-4);

    //tol[opt_problem.m_nic-1]=1e-2;
    prob.set_c_tol(tol);
    
    pagmo::snopt7 sn_v(false,"/usr/local/lib/");
    
    sn_v.set_integer_option("Verify level",-1);
    
    sn_v.set_integer_option("Scale option",0);
    //sn_v.set_integer_option("Violation limit",100);
    sn_v.set_numeric_option("Time limit",30);

    //sn_v.set_numeric_option("Scale tolerance",0.99);
    //sn_v.set_numeric_option("Elastic weight",100);
    
    //sn_v.set_numeric_option("Major step limit",0.1);
    //sn_v.set_numeric_option("Major iterations limit",500);
    //sn_v.set_numeric_option("Iterations limit",500);
    
    sn_v.set_numeric_option("Function precision",1e-10);

    algorithm algo{sn_v};
    algo.set_verbosity(100);
    //algo.file_output(true);
    std::cerr<<"Creating population"<<std::endl; 
    unsigned int init=2u;
    
    //std::cerr<<"Computing initial fitness from initial robot state"<<std::endl;
    vector<double> x_in(arm_dof*tim_st,0.5);
    if(req.initial_jtraj.points.size()==0)
    {
      for(int i=0;i<tim_st;++i)
      {
        for(int j=0;j<arm_dof;++j)
        {
          x_in[i*(arm_dof)+j]=req.arm_jstate.position[j];
        }

      }
    
    }
    else
    {
      cerr<<"initializing from traj"<<endl;
      for(int i=0;i<tim_st;++i)
      {
        for(int j=0;j<arm_dof;++j)
        {
          x_in[i*arm_dof+j] = req.initial_jtraj.points[req.initial_jtraj.points.size()-1].positions[j];//+(req.initial_jtraj.points[req.initial_jtraj.points.size()-1].positions[j]-req.initial_jtraj.points[0].positions[j])*double(i+1)/double(tim_st);
        }
      
      }
    }
    population pop{prob,init};
    cerr<<"computing initial fitness"<<endl;
    vector<double> f_in=prob.fitness(x_in);
    pop.set_xf(0,x_in,f_in);
 
    
    std::cerr<<"Runnnig snopt"<<std::endl;
    population pop_res=algo.evolve(pop);
    
    // get log
    //algo.extract(pagmo::snopt7()).get_log();
    //cerr<<prob<<endl;
    std::vector<double> final_pose;
    cerr<<"Final cost:"<<pop_res.champion_f()[0]<<endl;
    
    std::vector<double> x_best;
    std::vector<double> final_joint;
    
    x_best=pop_res.champion_x();

    trajectory_msgs::JointTrajectory joint_traj;
    trajectory_msgs::JointTrajectory arm_traj;
    trajectory_msgs::JointTrajectory hand_traj;
    arm_traj.joint_names = req.arm_jstate.name;
    hand_traj.joint_names = req.hand_jstate.name;    
    std::vector<std::string> joint_names=req.arm_jstate.name;
    joint_names.insert(joint_names.end(),req.hand_jstate.name.begin(),req.hand_jstate.name.end());
    joint_traj.joint_names=joint_names;

    // Starting point.
    // Robot trajectory.
    trajectory_msgs::JointTrajectoryPoint jt_pt;
    jt_pt.positions=joints_0;
    joint_traj.points.push_back(jt_pt);
    // Just the arm trajectory.
    trajectory_msgs::JointTrajectoryPoint arm_jt_pt;
    arm_jt_pt.positions=std::vector<double>(joints_0.begin(), joints_0.begin() + arm_dof);
    arm_traj.points.push_back(arm_jt_pt);
    // Just the hand trajectory.
    trajectory_msgs::JointTrajectoryPoint hand_jt_pt;
    hand_jt_pt.positions=std::vector<double>(joints_0.begin() + arm_dof, joints_0.end());
    hand_traj.points.push_back(hand_jt_pt);

    std::vector<double> joint_angles;
    joint_angles.resize(robot_kdl_.dof,0.0);
    std::vector<double> arm_angles;
    arm_angles.resize(arm_dof,0.0);
    std::vector<double> hand_angles;
    hand_angles.resize(hand_dof,0.0);
    
    for (int i=0;i<tim_st;i++)
    {       
      for(int j=0;j<arm_dof;j++)
      {
        arm_angles[j] = x_best[i*arm_dof+j];
        joint_angles[j] = x_best[i*arm_dof+j];
      }
      // add preshape:
      for(int j=0;j<hand_dof;j++)
      {
        hand_angles[j] = preshape[j];
        joint_angles[j+arm_dof] = preshape[j];
      }
      jt_pt.positions=joint_angles;
      joint_traj.points.push_back(jt_pt);
      arm_jt_pt.positions = arm_angles;
      arm_traj.points.push_back(arm_jt_pt);
      hand_jt_pt.positions = hand_angles;
      hand_traj.points.push_back(hand_jt_pt);

      ros::Duration dur = ros::Duration(float(i)*T/float(tim_st));
      jt_pt.time_from_start=dur;
      arm_jt_pt.time_from_start=dur;
      hand_jt_pt.time_from_start=dur;
    }

    res.robot_joint_traj=joint_traj;
    res.arm_joint_traj=arm_traj;
    res.hand_joint_traj=hand_traj;
    
    // final joint configuration:
    Eigen::VectorXd state(arm_dof);
    for(int j=0;j<arm_dof;j++)
    {
      state[j]=x_best[(tim_st-1)*arm_dof+j];
    }

    // get palm pose at final timestep:
    Eigen::VectorXd out_pose;
    robot_kdl_.getFK(reach_idx,state,out_pose);
    res.ee_pose.position.x=out_pose[0];
    res.ee_pose.position.y=out_pose[1];
    res.ee_pose.position.z=out_pose[2];
    res.ee_pose.orientation.x=out_pose[3];
    res.ee_pose.orientation.y=out_pose[4];
    res.ee_pose.orientation.z=out_pose[5];
    res.ee_pose.orientation.w=out_pose[6];
    
    return true;
  }
};

using namespace std;
int main(int argc, char** argv)
{
  //collisionChecker c_check;
  ros::init(argc,argv,"ll4ma_planner_node");
  ros::NodeHandle n;
  srvClass srv_call;
  srv_call.init(n);
  srv_call.init_robot_meshes();
  srv_call.init_env();
  srv_call.read_params();
  srv_call.init_obj();
  // create ros service:
  //ros::ServiceServer env_service=n.advertiseService( "/grasp_planner/update_world", &srvClass::update_env_cloud, &srv_call);
  ros::ServiceServer service=n.advertiseService("/ll4ma_planner/get_trajectory",&srvClass::plan_call,&srv_call);
  ros::ServiceServer service_t=n.advertiseService("/ll4ma_planner/update_world",&srvClass::world_call,&srv_call);
  ros::ServiceServer add_service=n.advertiseService("/ll4ma_planner/add_object",&srvClass::add_object_call,&srv_call);

  ROS_INFO("LL4MA planner service is ready");
  ros::Rate loop_rate(10);
  while(ros::ok())
  {
   
    loop_rate.sleep();
    ros::spinOnce();
  }
  //return 0;
}
                                               
 

 

