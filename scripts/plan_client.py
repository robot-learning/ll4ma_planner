import rospy
from ll4ma_planner.srv import GetEEPlan
from trajectory_msgs.msg import JointTrajectory
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point,Transform,Pose
import copy
import time
import numpy as np
#import tf
import sys
from rospkg import RosPack
rp=RosPack()
rp.list()
path=rp.get_path('trajectory_smoothing')+'/scripts'
sys.path.insert(0,path)
from trajectory_pub import *

class ll4maPlanner:
    def __init__(self,listen_prefix='/lbr4_allegro'):
        rospy.init_node('grasp_client')
        self.joint_sub=None
        self.got_state=False
        self.joint_sub=rospy.Subscriber(listen_prefix+'/joint_states',
                                        JointState,self.joint_state_cb,self.joint_sub)


        
        print 'initialized planner'
        
    def get_palm_plan(self,arm_j0,hand_j0,hand_shape,palm_pose,joint_traj,obj_pose,T=1):
        # get plan from service:
        #print 'waiting for grasp service'
        
        #print "waiting for call"
        rospy.wait_for_service('/ll4ma_planner/get_trajectory')
        planner=rospy.ServiceProxy('/ll4ma_planner/get_trajectory',GetEEPlan)
        #resp=planner(arm_j0,hand_j0,1,f_idx,cpts,JointTrajectory(),palm_poses,hand_shape,Pose(),True)
        resp=planner(arm_j0,hand_j0,1,[palm_pose],obj_pose,hand_shape,joint_traj,True)
        
        return resp.arm_joint_traj,resp.ee_pose
    def get_palm_traj(self,arm_j0,hand_j0,hand_shape,palm_pose,joint_traj,obj_pose,T=10):
        rospy.wait_for_service('/ll4ma_planner/get_trajectory')
        planner=rospy.ServiceProxy('/ll4ma_planner/get_trajectory',GetEEPlan)
        resp=planner(arm_j0,hand_j0,T,[palm_pose],obj_pose,hand_shape,joint_traj,True)

        return resp.arm_joint_traj,resp.ee_pose
    


    def joint_state_cb(self,joint_state):
        self.joint_state=joint_state
        self.got_state=True
        

    def viz_traj(self,j_traj,t_name='planner/ik_traj'):
        traj_server=trajectoryServer(100,robot_name='lbr4',topic_name=t_name,init_node=False)
        time.sleep(0.5)
        traj_server.viz_joint_traj(j_traj)

