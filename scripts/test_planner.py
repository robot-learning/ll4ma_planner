import rospy
from plan_client import * 
import sys
import tf
from trajectory_msgs.msg import JointTrajectory
from geometry_msgs.msg import PoseStamped
from rospkg import RosPack

rp=RosPack()
rp.list()
path=rp.get_path('trajectory_smoothing')+'/scripts'
sys.path.insert(0,path)
from trajectory_pub import *
path=rp.get_path('ll4ma_opt_utils')+'/scripts'
sys.path.insert(0,path)
from tf_helper import tfHelper
from robot_class import robotInterface
if __name__=='__main__':
    g_plan=ll4maPlanner()
    #hand_client=handClient()
    #lbr4_client=robotInterface(init_node=False)
    rate=rospy.Rate(10)
    obj_name="soft_scrub"
    tf_helper=tfHelper()
    tf_listener=tf.TransformListener()
    while(not g_plan.got_state):
        rate.sleep()
    print "ready"
    robot_js=JointState()
    robot_js.name=['lbr4_j0','lbr4_j1','lbr4_j2','lbr4_j3','lbr4_j4','lbr4_j5','lbr4_j6']
    robot_js.position=g_plan.joint_state.position[0:7]

    
    hand_js=JointState()
    hand_js.name=['index_joint_0','index_joint_1','index_joint_2','index_joint_3',
                  'middle_joint_0','middle_joint_1', 'middle_joint_2','middle_joint_3',
                  'ring_joint_0', 'ring_joint_1', 'ring_joint_2', 'ring_joint_3',
                  'thumb_joint_0', 'thumb_joint_1', 'thumb_joint_2','thumb_joint_3']
    hand_js.position=g_plan.joint_state.position[7:]

    hand_preshape=copy.deepcopy(hand_js)
    hand_preshape.position=np.array([0.0,0.0,0.3,0.3, 0.0, 0.0, 0.3, 0.3,
                                     0.0,0.0,0.3,0.3, 1.4,0.0,0.0,0.0])

    raw_input('Plan grasp traj?')
    palm_pose=Pose()
    palm_pose.position.x=-0.4
    palm_pose.position.y=-0.5
    palm_pose.position.z=0.3
    palm_pose.orientation.x=0.645
    palm_pose.orientation.y=-0.394
    palm_pose.orientation.z=-0.405

    palm_pose.orientation.w=0.514
    obj_pose=Pose()
    obj_pose.position.x=-0.3
    obj_pose.position.y=-0.5

    obj_pose.position.z=0.7
    obj_pose.orientation.w=1.0
   
    start = time.time()
    #robot_traj,palm_pose =g_plan.get_palm_plan(robot_js,hand_js,hand_preshape,palm_pose,JointTrajectory(),obj_pose,T=1)
    robot_traj, palm_pose=g_plan.get_palm_traj(robot_js,hand_js,hand_preshape,palm_pose,JointTrajectory(),obj_pose,T=10)

    end = time.time()
    
    print 'Traj Planning took: '+ str(end - start) +' seconds'
    g_plan.viz_traj(robot_traj,t_name='ll4ma_planner/traj')
