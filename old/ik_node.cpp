#include "ll4ma_planner/problems/opt_problem.hpp"
#include "ll4ma_planner/pagmo_opt/trajectory_optimization.hpp"

// Robot kdl:
#include <ros/package.h>
#include <ros/ros.h>

// ROS service:
#include "ll4ma_planner/IKJointTrajectory.h"

// joint trajectory
#include <trajectory_msgs/JointTrajectory.h>
using namespace pagmo;


class srvClass
{
private:
  // parameters:
  manipulator_kdl::robotKDL robot_kdl_;//("robot_description",*nh,base_names,ee_names,g_vec);

public:
  ros::NodeHandle* nh;
  //int dof_;
  void init(ros::NodeHandle &n)
  {
    nh=&n;
    string urdf_param;
    assert(nh->getParam("urdf_param",urdf_param));
    
    while(!nh->hasParam(urdf_param) && ros::ok())
    {
      //loop_rate.sleep();
    }
    // default params:
    vector<string> ee_names={"lbr4_7_link"};
    vector<string> base_names={"base_link"};
    
    assert(nh->getParam("base_link",base_names[0]));
    assert(nh->getParam("ee_link",ee_names[0]));

    // robot link names:
    vector<double> g_vec={0.0,0.0,-9.8};
    robot_kdl_=manipulator_kdl::robotKDL(urdf_param,*nh,base_names,ee_names,g_vec);
    robot_kdl_.getJointLimits(0,robot_kdl_.up_bounds,robot_kdl_.low_bounds);
    //robot_kdl_.urdfParser();
    //dof_=robot_kdl_.dof;

  }
  bool ik_call(ll4ma_planner::IKJointTrajectory::Request &req,ll4ma_planner::IKJointTrajectory::Response &res)
  {

    // Desired pose:
    vector<double> des_pose;
    des_pose.resize(6);    
    des_pose[0]=req.desired_pose[0];
    des_pose[1]=req.desired_pose[1];
    des_pose[2]=req.desired_pose[2];
    des_pose[3]=req.desired_pose[3];
    des_pose[4]=req.desired_pose[4];
    des_pose[5]=req.desired_pose[5];

    // Initial robot state:
    vector<double> joints_0;
    joints_0.resize(robot_kdl_.dof);
    Eigen::VectorXd j_0;
    j_0.resize(robot_kdl_.dof);
    
    for(int i=0;i<req.initial_joint_angles.size();++i)
    {
      joints_0[i]=req.initial_joint_angles[i];
      j_0[i]=joints_0[i];
    }
    
    // Get initial object pose:
    Eigen::VectorXd initial_pose_;
    robot_kdl_.getFK(0,j_0,initial_pose_,true);
    vector<double> initial_pose(initial_pose_.data(),initial_pose_.data()+initial_pose_.size());
    vector<vector<double>> data_arr;
    data_arr.resize(2);
    data_arr[0]=des_pose;
    data_arr[1]=initial_pose;
    
    
    vector<double> vel_limits;
    
    // check timesteps:
    int t_steps=req.t_steps;
    if(t_steps>1)
    {
      vel_limits.resize(robot_kdl_.dof,0.6);
    }
    else
    {
      vel_limits.resize(robot_kdl_.dof,10000.0);
    }
    
    // Create a opt problem instance
    opt_problems::optProblem opt_problem(t_steps,robot_kdl_,data_arr,vel_limits,joints_0);
    
   
    problem prob{trajectory_optimization{opt_problem}};
    vector<double> tol(robot_kdl_.dof*(t_steps),1e-3);
    
    prob.set_c_tol(tol);
    pagmo::snopt7 sn_v(false,"/usr/local/lib/");
    sn_v.set_numeric_option("Major optimality tolerance",req.obj_tol); 
 
    //sn_v.set_integer_option("Iterations limit",100);
    //sn_v.set_numeric_option("Linesearch tolerance",0.1);
    
    algorithm algo{sn_v};
    population pop{prob,10u};

    // initialize with current joint angles:
    vector<double> x_in(robot_kdl_.dof*t_steps,0.0);
    // linear interpolation if final joints are available:
    if(req.final_joint_angles.size()!=0)
    {
      //cerr<<"Initializing from IK"<<endl;
       for(int i=0;i<t_steps;++i)
      {
        for (int j=0;j<robot_kdl_.dof;++j)
        {
          x_in[i*robot_kdl_.dof+j]=joints_0[j];
        }
        if(i==t_steps-1)
        {
          for (int j=0;j<robot_kdl_.dof;++j)
          {
            x_in[i*robot_kdl_.dof+j]=req.final_joint_angles[j];
          }
          
        }
      }
    }
    else
    {
      for(int i=0;i<t_steps;++i)
      {
        for (int j=0;j<robot_kdl_.dof;++j)
        {
          x_in[i*robot_kdl_.dof+j]=joints_0[j];
        }
      }
    }
  vector<double> f_in=prob.fitness(x_in);
  pop.set_xf(0,x_in,f_in); 
  cerr<<"Initial fitness: "<<f_in[0]<<endl;
  cerr<<"Solving"<<endl;
  population pop_res = algo.evolve(pop);

  std::vector<double> final_pose;
  cerr<<"Final cost:"<<pop_res.champion_f()[0]<<endl;
  
  std::vector<double> x_best;
  std::vector<double> final_joint;
  x_best.resize(robot_kdl_.dof,0.0);
  final_joint.resize(robot_kdl_.dof,0.0);
  x_best=pop_res.champion_x();

  trajectory_msgs::JointTrajectory joint_traj;
  std::vector<std::string> joint_names=robot_kdl_.getJointNames(0);
  joint_traj.joint_names=joint_names;    
  std::vector<double> joint_angles;
  joint_angles.resize(robot_kdl_.dof,0.0);
  trajectory_msgs::JointTrajectoryPoint jt_pt0;
  jt_pt0.positions=joints_0;
  joint_traj.points.push_back(jt_pt0);

  for (int i=0;i<t_steps;i++)
  { 
    trajectory_msgs::JointTrajectoryPoint jt_pt;
    for(int j=0;j<robot_kdl_.dof;j++)
    {
      joint_angles[j]=x_best[i*robot_kdl_.dof+j];
    }
    jt_pt.positions=joint_angles;
    joint_traj.points.push_back(jt_pt);
  }
  res.collision_free=true;
  res.joint_traj=joint_traj;
  res.champion_f=pop_res.champion_f();

  return true;
}
};

int main(int argc, char** argv)
{

  ros::init(argc,argv,"ik_planner_server");
  ros::NodeHandle n;
  srvClass srv_call;
  
  // Read problem parameters:

  srv_call.init(n);
  // create ros service:
  ros::ServiceServer service=n.advertiseService("ik_planner",&srvClass::ik_call,&srv_call);
  ROS_INFO("IK service ready");

  ros::spin();
}
                                               
 
