//#ifndef SPARSE_GRADIENT
#define SPARSE_GRADIENT true
//#endif
#include "ll4ma_planner/problems/opt_problem.hpp"

using namespace opt_problems;
optProblem::optProblem(int dummy)
{
  std::cerr<<"optProblem dummy constructor is initialized, fix me!!!"<<std::endl;
}

optProblem::optProblem(int time_steps,manipulator_kdl::robotKDL &robot_kdl, vector<vector<double>> data_arr,vector<double> vel_limits,vector<double> init_joint_angles)
{
  // Get optimization dimensions from robot kdl:
  m_dim=robot_kdl.dof*time_steps;
  m_nec=0;//robot_kdl.dof;
  m_nic=robot_kdl.dof*(time_steps);// velocity constraints
  timesteps_=time_steps;
  robot_kdl_=&robot_kdl;
  des_poses.resize(time_steps);
  joints_0=init_joint_angles;
  vel_lim=vel_limits[0];
  
  Map<Eigen::VectorXd> des_pose(&data_arr[0][0],data_arr[0].size());// desired object pose
  Map<Eigen::VectorXd> initial_pose(&data_arr[1][0],data_arr[1].size());

  for(int i=0;i<time_steps;++i)
  {
    des_poses[i]=des_pose;
  }
}

 
double optProblem::objFunction(const vector<double> x) const
{
  double wa=0.1;
  double wd=100.0;
  // Cost function is on reaching a desired cartesian pose 
  double cost_val=0.0;
  Eigen::VectorXd q;
  vector<double> ee_pose_arr;
  Eigen::VectorXd pos_err,ee_pose;
  double orient_err=0.0;
  Eigen::VectorXd acc_cost(robot_kdl_->dof);
  
  q.resize(robot_kdl_->dof);


  vector<double> o_err;

  vector<Eigen::VectorXd> q_arr;
  Eigen::VectorXd j0(joints_0.size());
  for(int i=0;i<joints_0.size();++i)
  {
    j0[i]=joints_0[i];
  }
  //Map<Eigen::VectorXd> j0(&joints_0[0], joints_0.size());
  
  q_arr.push_back(j0);
  q_arr.push_back(j0);
  
  for(int i=0; i<timesteps_;++i)
  {
    for(int j=0; j<robot_kdl_->dof;++j)
    {
      q[j]=x[(i)*robot_kdl_->dof+j];
    }
    q_arr.push_back(q);
  }
  q_arr.push_back(q);
  q_arr.push_back(q);


  
  for(int i=0; i<timesteps_;++i)
  {
    // min acc cost:
    acc_cost=q_arr[i-2+2]-2.0*q_arr[i+2-1]+q_arr[i+2];
    cost_val+=wa*acc_cost.squaredNorm();
    q=q_arr[i+2];
    
    if(i==timesteps_-1)
    {
      vector<double> o_d(des_poses[i].data()+3, des_poses[i].data()+6);    
      robot_kdl_->getFK(0,q,ee_pose,true);
      
      pos_err=des_poses[i]-ee_pose;
      
      vector<double> o_1(ee_pose.data()+3, ee_pose.data()+6);
      
      robot_kdl_->euler_diff(o_1,o_d,o_err);
      pos_err[3]=o_err[0]*0.1;
      pos_err[4]=o_err[1]*0.1;
      pos_err[5]=o_err[2]*0.1;
      cost_val+=wd*pos_err.squaredNorm();
    }

  }
  return cost_val;
}
vector<double> optProblem::objGradient(vector<double> x) const
{
  vector<double> retval(m_dim,0.0);
  Eigen::VectorXd pos_err,q,ee_pose;
  Eigen::VectorXd gradient,acc_cost,acc_grad;
  Eigen::MatrixXd J;
  gradient.resize(7);
  gradient.setZero();
  q.resize(robot_kdl_->dof);
  double wa=0.1;
  double wd=100.0;
  vector<Eigen::VectorXd> q_arr;
  Eigen::VectorXd j0(joints_0.size());
  for(int i=0;i<joints_0.size();++i)
  {
    j0[i]=joints_0[i];
  }

  q_arr.push_back(j0);
  q_arr.push_back(j0);
  

  for(int i=0; i<timesteps_;++i)
  {
    for(int j=0; j<robot_kdl_->dof;++j)
    {
      q[j]=x[(i)*robot_kdl_->dof+j];
    }
    q_arr.push_back(q);
  }
  q_arr.push_back(q);
  q_arr.push_back(q);


  vector<double> o_err;

  
  for(int i=0;i<timesteps_;++i)
  {
    q=q_arr[i+2];
    gradient.setZero();
    // min acc grad:
    
    
    acc_cost=wa*(q_arr[i-2+2]-2.0*q_arr[i+2-1]+q_arr[i+2]);
    
    
    acc_grad=2.0*acc_cost;
    for (int j=0;j<robot_kdl_->dof;++j)
    {
      retval[j+robot_kdl_->dof*(i)]+=acc_grad[j];
    }

    if(i>1)
    {
      acc_grad=2.0*acc_cost;
      for (int j=0;j<robot_kdl_->dof;++j)
      {
        retval[j+robot_kdl_->dof*(i-2)]+=acc_grad[j];
      } 
    }
    if(i>0)
    {
      acc_grad=-4.0*acc_cost;
      for (int j=0;j<robot_kdl_->dof;++j)
      {
        retval[j+robot_kdl_->dof*(i-1)]+=acc_grad[j];
      }
     
    }
    
    if(i==timesteps_-1)
    {
      acc_cost=wa*(q_arr[i-1+2]-q_arr[i+2]);
      
      acc_grad=2.0*acc_cost;
      for (int j=0;j<robot_kdl_->dof;++j)
      {
        retval[j+robot_kdl_->dof*(i-1)]+=acc_grad[j];
      }
      acc_grad=-2.0*acc_cost;
      for (int j=0;j<robot_kdl_->dof;++j)
      {
        retval[j+robot_kdl_->dof*(i)]+=acc_grad[j];
      }

    }
    
    if(i==timesteps_-1)
    {
      vector<double> o_d(des_poses[i].data()+3, des_poses[i].data()+6);
      
      robot_kdl_->getFK(0,q,ee_pose,true);
    
  
  
      pos_err=des_poses[i]-ee_pose;
    
      // Convert RPY to quaternion:
      vector<double> o_1(ee_pose.data()+3, ee_pose.data()+6);
      
      robot_kdl_->euler_diff(o_1,o_d,o_err);
      pos_err[3]=o_err[0]*0.1;
      pos_err[4]=o_err[1]*0.1;
      pos_err[5]=o_err[2]*0.1;
      
      robot_kdl_->getJacobian(0,q,J);
      gradient+=-2.0*wd*pos_err.transpose()*J;
    }
    
    for (int j=0;j<robot_kdl_->dof;++j)
    {
      retval[j+robot_kdl_->dof*(i)]+=gradient[j];
    }
  }
  
  return retval;
}

vector<double> optProblem::inEqConstraints(vector<double> x) const
{
  // limiting velocity at joints
  vector<double> vel_const(m_nic,0.0);

  // Encoding initial joint angle constraint as velocity constraint

 
  for(int j=0;j<robot_kdl_->dof;++j)
  {
    vel_const[(0)*robot_kdl_->dof+j]=std::abs(x[(0)*robot_kdl_->dof+j]-joints_0[j])-vel_lim;
  }

  for (int i=1; i<timesteps_-1; ++i)
  {
    for(int j=0;j<robot_kdl_->dof;++j)
    {
      vel_const[(i)*robot_kdl_->dof+j]=std::abs(x[(i)*robot_kdl_->dof+j]-x[(i-1)*robot_kdl_->dof+j])-vel_lim;
    }
  }

  return vel_const;
}

vector<double> optProblem::inEqGradient(vector<double> x) const
{
   vector<double> retval(1*robot_kdl_->dof+2*(m_nic-robot_kdl_->dof),0.0);

   
   for(int j=0;j<robot_kdl_->dof;++j)
  {
    if(x[j]-joints_0[j]<0.0)
    {
      retval[j]=-1.0;
    }
    else
    {
      retval[j]=1.0;
    }
  }
 
  for(int i=1;i<timesteps_;++i)
  {
      for(int j=0;j<robot_kdl_->dof;++j)
      {
        if(x[(i)*robot_kdl_->dof+j]-x[(i-1)*robot_kdl_->dof+j]<0.0)
        {
          retval[1*robot_kdl_->dof+2*(i-1)*robot_kdl_->dof+j]=1.0;
          
          retval[1*robot_kdl_->dof+2*(i-1)*robot_kdl_->dof+robot_kdl_->dof+j]=-1.0;

        }
        else
        {
          retval[1*robot_kdl_->dof+2*(i-1)*robot_kdl_->dof+j]=-1.0;
                                                                
          retval[1*robot_kdl_->dof+2*(i-1)*robot_kdl_->dof+robot_kdl_->dof+j]=1.0;
          
        }
      }
  }
  return retval;
}

vector<double> optProblem::EqConstraints(vector<double> x) const
{
  // no constraint
  vector<double> ret(m_nec,0.0);
  return  ret;
  
}



vector<double> optProblem::eqGradient(vector<double> x) const
{
  vector<double> retval(m_dim*m_nec,0.0);
  return retval;
}

vector<vector<double>> optProblem::bounds() const
{
  vector<vector<double>> bound;
  vector<double> up_bounds;
  vector<double> low_bounds;
  
  // Getting joint angle limits from urdf
  for(int i=0;i<timesteps_;++i)
  {
    up_bounds.insert(up_bounds.end(), robot_kdl_->up_bounds.begin(), robot_kdl_->up_bounds.end());
    low_bounds.insert(low_bounds.end(), robot_kdl_->low_bounds.begin(), robot_kdl_->low_bounds.end());
  }

  bound.resize(2);
  bound[0]=low_bounds;
  bound[1]=up_bounds;
  return bound;
  
}

//#if SPARSE_GRADIENT==false
vector<double> optProblem::gradient(const vector<double> x) const
{
  vector<double> grad;
  vector<double> obj_grad=objGradient(x);
  vector<double> eq_grad=eqGradient(x);
  vector<double> ineq_grad=inEqGradient(x);
  
  grad.resize(m_dim+m_dim*m_nec+m_dim*m_nic,0.0);

  for(int i=0;i<m_dim;i++)
  {
    grad[i]=obj_grad[i];

  }

  for(int i=m_dim;i<m_dim+m_dim*m_nec;i++)
  {
    grad[i]=eq_grad[i-m_dim];
  }


  for(int i=m_dim+m_dim*m_nec;i<m_dim+m_dim*m_nec+m_dim*m_nic;i++)
  {
    
    grad[i]=ineq_grad[i-m_dim-m_dim*m_nec];
  }

  return grad;
}
//#endif
#if SPARSE_GRADIENT==true
std::vector<std::pair<vector<double>::size_type, vector<double>::size_type>> optProblem::gradient_sparsity() const
{
  std::vector<std::pair<vector<double>::size_type, vector<double>::size_type>> retval;
  // Objective gradient is dense
  for (int i=0;i<m_dim;++i)
  {
    retval.emplace_back(0,i);
  }
  // inequality constraints:
  for(int i=0;i<robot_kdl_->dof;++i)
  {
    retval.emplace_back(1+m_nec+i,i);
  }
  // Other timesteps:
  //for(int i=0;
  for(int i=1;i<timesteps_;++i)
  {
    for(int j=0;j<robot_kdl_->dof;++j)
    {
      retval.emplace_back(1+m_nec+1*robot_kdl_->dof+(i-1)*robot_kdl_->dof+j, (i-1)*robot_kdl_->dof+j);
      retval.emplace_back(1+m_nec+1*robot_kdl_->dof+(i-1)*robot_kdl_->dof+j,(i)*robot_kdl_->dof+j);
    }
  }
  return retval;
}
vector<double> optProblem::sparse_gradient(const vector<double> x) const
{
  vector<double> grad;
  vector<double> obj_grad=objGradient(x);
  //vector<double> eq_grad=eqGradient(x);
  vector<double> ineq_grad=inEqGradient(x);
  
  grad.resize(m_dim,0.0);

  for(int i=0;i<m_dim;i++)
  {
    grad[i]=obj_grad[i];
  }

  for(int j=0;j<robot_kdl_->dof;++j)
  {
    grad.emplace_back(ineq_grad[j]);
  }
  
  for(int i=1;i<timesteps_;++i)
  {
    for(int j=0;j<robot_kdl_->dof;++j)
    {
      grad.emplace_back(ineq_grad[1*robot_kdl_->dof+2*(i-1)*robot_kdl_->dof+j]);
      grad.emplace_back(ineq_grad[1*robot_kdl_->dof+2*(i-1)*robot_kdl_->dof+robot_kdl_->dof+j]);
    }
  }

  return grad;
}
#endif
