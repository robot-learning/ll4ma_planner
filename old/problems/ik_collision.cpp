//#define COLLISION_CHECKING true
#define SPARSE_GRADIENT true
#define IK_PLANNER true
#include "ll4ma_planner/problems/collision_opt_problem.hpp"
using namespace opt_problems;
optProblem::optProblem(int dummy)
{
  std::cerr<<"optProblem dummy constructor is initialized, fix me!!!"<<std::endl;
}
optProblem::optProblem(int time_steps, manipulator_kdl::robotKDL &robot_kdl, robEnv &c_check, vector<double> des_pose,vector<double> vel_limits,vector<double> init_joint_angles)
{
  m_dim=robot_kdl.dof*time_steps;
  m_nec=0;//
  m_nic=robot_kdl.dof*(time_steps)+time_steps;// velocity constraints
  timesteps_=time_steps;
  robot_kdl_=&robot_kdl;
  joints_0=init_joint_angles;
  vel_lim=vel_limits[0];
  
  Map<Eigen::VectorXd> des_pose_t(&des_pose[0],des_pose.size());// desired object pose
  des_pose_=des_pose_t;
  cerr<<"initialized optproblem"<<endl;
  c_checker=c_check;
  if(timesteps_>1)
  {
    wa=0.1;
    wm=0.0001;
  }
  else
  {
    wa=0.0;
    wm=0.001;
  }
  wd=10.0;
  j_thresh=0.2;
  n_links_=robot_kdl_->getNumLinks();
}

double optProblem::objFunction(const vector<double> x) const
{
  // Cost function is on reaching a desired cartesian pose

  // Cost function is on reaching a desired cartesian pose 
  double cost_val=0.0;
  double mid_cost=0.0;
  Eigen::VectorXd jnt_mid(robot_kdl_->dof);
  Eigen::VectorXd q;
  vector<double> ee_pose_arr;
  Eigen::VectorXd pos_err,ee_pose,coll_err;
  double orient_err=0.0;
  Eigen::VectorXd acc_cost(robot_kdl_->dof);
  
  q.resize(robot_kdl_->dof);


  vector<double> o_err;

  vector<Eigen::VectorXd> q_arr;
  Eigen::VectorXd j0(joints_0.size());
  for(int i=0;i<joints_0.size();++i)
  {
    j0[i]=joints_0[i];
  }

  
  q_arr.push_back(j0);
  q_arr.push_back(j0);
  
  for(int i=0; i<timesteps_;++i)
  {
    for(int j=0; j<robot_kdl_->dof;++j)
    {
      q[j]=x[(i)*robot_kdl_->dof+j];
    }
    q_arr.push_back(q);
  }
  q_arr.push_back(q);
  q_arr.push_back(q);

  for(int i=0; i<timesteps_;++i)
  {
    // min acc cost:
    acc_cost=q_arr[i-2+2]-2.0*q_arr[i+2-1]+q_arr[i+2];
    cost_val+=wa*acc_cost.squaredNorm();
    q=q_arr[i+2];
    for(int j=1;j<5;++j)
    {
      cost_val+=wm*(exp(max(std::abs(q[j]),robot_kdl_->up_bounds[j]-j_thresh))-exp(robot_kdl_->up_bounds[j]-j_thresh));
    }

    
    if(i==timesteps_-1)
    {
      // desired pose error:
      vector<double> o_d(des_pose_.data()+3, des_pose_.data()+6);    
      robot_kdl_->getFK(0,q,ee_pose,true);
      
      pos_err=des_pose_-ee_pose;
      
      vector<double> o_1(ee_pose.data()+3, ee_pose.data()+6);
      
      robot_kdl_->euler_diff(o_1,o_d,o_err);
      pos_err[3]=o_err[0]*0.1;
      pos_err[4]=o_err[1]*0.1;
      pos_err[5]=o_err[2]*0.1;
      cost_val+=wd*pos_err.squaredNorm();
    }
  }
  
  return cost_val;
}
vector<double> optProblem::objGradient(vector<double> x) const
{
  vector<double> retval(m_dim,0.0);
  Eigen::VectorXd pos_err,q,ee_pose;
  Eigen::VectorXd gradient,acc_cost,acc_grad;
  Eigen::MatrixXd J;
  gradient.resize(7);
  gradient.setZero();
  q.resize(robot_kdl_->dof);

  vector<Eigen::VectorXd> q_arr;
  Eigen::VectorXd j0(joints_0.size());
  for(int i=0;i<joints_0.size();++i)
  {
    j0[i]=joints_0[i];
  }

  q_arr.push_back(j0);
  q_arr.push_back(j0);
  

  for(int i=0; i<timesteps_;++i)
  {
    for(int j=0; j<robot_kdl_->dof;++j)
    {
      q[j]=x[(i)*robot_kdl_->dof+j];
    }
    q_arr.push_back(q);
  }
  q_arr.push_back(q);
  q_arr.push_back(q);


  vector<double> o_err;
  
  for(int i=0;i<timesteps_;++i)
  {
    q=q_arr[i+2];
    gradient.setZero();
    // min acc grad:
    acc_cost=wa*(q_arr[i-2+2]-2.0*q_arr[i+2-1]+q_arr[i+2]);
    acc_grad=2.0*acc_cost;
    for (int j=0;j<robot_kdl_->dof;++j)
    {
      retval[j+robot_kdl_->dof*(i)]+=acc_grad[j];
    }
    if(i>1)
    {
      acc_grad=2.0*acc_cost;
      for (int j=0;j<robot_kdl_->dof;++j)
      {
        retval[j+robot_kdl_->dof*(i-2)]+=acc_grad[j];
      } 
    }
    if(i>0)
    {
      acc_grad=-4.0*acc_cost;
      for (int j=0;j<robot_kdl_->dof;++j)
      {
        retval[j+robot_kdl_->dof*(i-1)]+=acc_grad[j];
      }
     
    }

    // mid point gradient:
    for(int j=1;j<5;++j)
    {
      retval[j+robot_kdl_->dof*(i)]+=wm*q[j]*(exp(max(std::abs(q[j]),robot_kdl_->up_bounds[j]-j_thresh))-exp(robot_kdl_->up_bounds[j]-j_thresh));
    }

   
    if(i==timesteps_-1)
    {
      acc_cost=wa*(q_arr[i-1+2]-q_arr[i+2]);
      
      acc_grad=2.0*acc_cost;
      for (int j=0;j<robot_kdl_->dof;++j)
      {
        retval[j+robot_kdl_->dof*(i-1)]+=acc_grad[j];
      }
      acc_grad=-2.0*acc_cost;
      for (int j=0;j<robot_kdl_->dof;++j)
      {
        retval[j+robot_kdl_->dof*(i)]+=acc_grad[j];
      }

    }



    
    if(i==timesteps_-1)
    {
      
      vector<double> o_d(des_pose_.data()+3, des_pose_.data()+6);
      robot_kdl_->getFK(0,q,ee_pose,true);
      pos_err=des_pose_-ee_pose;
      // Convert RPY to quaternion:
      vector<double> o_1(ee_pose.data()+3, ee_pose.data()+6);
      robot_kdl_->euler_diff(o_1,o_d,o_err);
      pos_err[3]=o_err[0]*0.1;
      pos_err[4]=o_err[1]*0.1;
      pos_err[5]=o_err[2]*0.1;
    
      robot_kdl_->getJacobian(0,q,J);
      gradient+=-2.0*wd*pos_err.transpose()*J;
    }
    
    
    for (int j=0;j<robot_kdl_->dof;++j)
    {
      retval[j+robot_kdl_->dof*(i)]+=gradient[j];
    }
  }
  
  return retval;

}

vector<double> optProblem::inEqConstraints(vector<double> x) const
{
    vector<double> vel_const(m_nic,0.0);


  for(int j=0;j<robot_kdl_->dof;++j)
  {
    vel_const[(0)*robot_kdl_->dof+j]=std::abs(x[(0)*robot_kdl_->dof+j]-joints_0[j])-vel_lim;
  }

  for (int i=1; i<timesteps_-1; ++i)
  {
    for(int j=0;j<robot_kdl_->dof;++j)
    {
      vel_const[(i)*robot_kdl_->dof+j]=std::abs(x[(i)*robot_kdl_->dof+j]-x[(i-1)*robot_kdl_->dof+j])-vel_lim;
    }
  }
  // collision const:
  vector<vector<double>> r_poses;
  vector<coll_data> c_data;
  Eigen::VectorXd q;
  q.resize(robot_kdl_->dof);
  for(int i=0;i<timesteps_;++i)
  {
    // collision checker variables:
    double collision_err=0.0;
    
    for(int j=0; j<robot_kdl_->dof;++j)
    {
      q[j]=x[(i)*robot_kdl_->dof+j];
    }
    
    robot_kdl_->getLinkPoses(q,r_poses);
    
    c_checker.rob_env_ccheck(r_poses,c_data);
    
    for(int j=0;j<robot_kdl_->dof;++j)
    {
      if(c_data[j].distance<0.0)
      {
        collision_err+=1.0*std::abs(c_data[j].distance);
      }
      else
      {
        if(c_data[j].distance<0.001)
        {
          //collision_err+=1.0*std::abs(c_data[j].distance);
        }
      }
      
    }
    vel_const[robot_kdl_->dof*timesteps_+i]=collision_err;

  }
    return vel_const;
}
vector<double> optProblem::inEqGradient(vector<double> x) const
{
  vector<double> retval(1*robot_kdl_->dof+2*(m_nic-robot_kdl_->dof),0.0);

   
   for(int j=0;j<robot_kdl_->dof;++j)
  {
    if(x[j]-joints_0[j]<0.0)
    {
      retval[j]=-1.0;
    }
    else
    {
      retval[j]=1.0;
    }
  }
  
  for(int i=1;i<timesteps_;++i)
  {
      for(int j=0;j<robot_kdl_->dof;++j)
      {
        if(x[(i)*robot_kdl_->dof+j]-x[(i-1)*robot_kdl_->dof+j]<0.0)
        {
          retval[1*robot_kdl_->dof+2*(i-1)*robot_kdl_->dof+j]=1.0;
          
          retval[1*robot_kdl_->dof+2*(i-1)*robot_kdl_->dof+robot_kdl_->dof+j]=-1.0;

        }
        else
        {
          retval[1*robot_kdl_->dof+2*(i-1)*robot_kdl_->dof+j]=-1.0;
                                                                
          retval[1*robot_kdl_->dof+2*(i-1)*robot_kdl_->dof+robot_kdl_->dof+j]=1.0;
          
        }
      }
  }
  return retval;
}
vector<double> optProblem::EqConstraints(vector<double> x) const
{
  // no constraint
  vector<double> ret(m_nec,0.0);
  
  return  ret;  
}

vector<double> optProblem::eqGradient(vector<double> x) const
{
  vector<double> retval(m_dim*m_nec,0.0);
  
  return retval;
}

vector<double> optProblem::gradient(const vector<double> x) const
{
  vector<double> grad;
  vector<double> obj_grad=objGradient(x);
  vector<double> eq_grad=eqGradient(x);
  vector<double> ineq_grad=inEqGradient(x);
  
  grad.resize(m_dim+m_dim*m_nec+m_dim*m_nic,0.0);

  for(int i=0;i<m_dim;i++)
  {
    grad[i]=obj_grad[i];

  }

  for(int i=m_dim;i<m_dim+m_dim*m_nec;i++)
  {
    grad[i]=eq_grad[i-m_dim];
  }


  for(int i=m_dim+m_dim*m_nec;i<m_dim+m_dim*m_nec+m_dim*m_nic;i++)
  {
    
    grad[i]=ineq_grad[i-m_dim-m_dim*m_nec];
  }

  return grad;
}
vector<vector<double>> optProblem::bounds() const
{
  vector<vector<double>> bound;
  vector<double> up_bounds;
  vector<double> low_bounds;
  
  // Getting joint angle limits from urdf
  for(int i=0;i<timesteps_;++i)
  {
    up_bounds.insert(up_bounds.end(), robot_kdl_->up_bounds.begin(), robot_kdl_->up_bounds.end());
    low_bounds.insert(low_bounds.end(), robot_kdl_->low_bounds.begin(), robot_kdl_->low_bounds.end());
  }

  bound.resize(2);
  bound[0]=low_bounds;
  bound[1]=up_bounds;
  return bound;
  
}
vector<double> optProblem::collisionGradient(vector<double> x) const
{
  vector<double> retval(robot_kdl_->dof*timesteps_,0.0);

  vector<vector<double>> r_poses;
  vector<coll_data> c_data;
  Eigen::VectorXd q;
  q.resize(robot_kdl_->dof);
  Eigen::MatrixXd link_J;
  Eigen::VectorXd gradient;
  gradient.resize(7);
  gradient.setZero();

  for(int i=0;i<timesteps_;++i)
  {
    // collision checker variables:
    
    for(int j=0; j<robot_kdl_->dof;++j)
    {
      q[j]=x[(i)*robot_kdl_->dof+j];
    }
    robot_kdl_->getLinkPoses(q,r_poses);
    //cerr<<r_poses[1][6]<<endl;
    c_checker.rob_env_ccheck(r_poses,c_data);
    gradient.setZero();

    for(int j=0;j<n_links_;++j)
    {
      //cerr<<"Gradient: Computing link "<<j<< " Jacobian"<<endl;
      robot_kdl_->getLinkJacobian(j,q,link_J);
      //cerr<<j<<' '<<link_J<<endl;
      //cerr<<"Gradient: Computed link Jacobian"<<endl;
      Map<Eigen::VectorXd> link_pose(&r_poses[j][0],3);
      //cerr<<"Gradient: Copied link pose"<<endl;
      int n_js=link_J.cols();
      //cerr<<c_data[j].distance<<endl;

      if(c_data[j].distance<0.0)
      {
        Eigen::VectorXd delta_p=c_data[j].p2-c_data[j].p1;

        Eigen::MatrixXd j_mat;
        j_mat=compute_jacobian(j,q,c_data[j].p1);
        gradient.segment(0,n_js)+=1.0*copysign(1.0,c_data[j].distance)*(j_mat*delta_p).segment(0,n_js);
        //finger_gradient.segment(0,n_js)+=1.0*copysign(1.0, s_d)*(j_mat*delta_p).segment(0,n_js);

        
        //delta_p=delta_p/delta_p.lpNorm<2>();
        //gradient.segment(0,n_js)+=1.0*c_data[j].distance*delta_p.transpose()*link_J.block(0,0,3,n_js);
        
      }
      else
      {
        double p_d=c_data[j].distance;
        if(p_d<0.001)
        {
          Eigen::VectorXd delta_p=c_data[j].p2-c_data[j].p1;
          Eigen::MatrixXd j_mat;
          j_mat=compute_jacobian(j,q,c_data[j].p1);
          //gradient.segment(0,n_js)+=1.0*copysign(1.0,c_data[j].distance)*(j_mat*delta_p).segment(0,n_js);
          /*
          delta_p=delta_p/delta_p.lpNorm<2>();
          gradient.segment(0,n_js)+=-1.0*p_d*delta_p.transpose()*link_J.block(0,0,3,n_js);
          */
        }
      }
      
    }

    // add robot collision gradient to main
    for(int k=0;k<robot_kdl_->dof;++k)
    {
      retval[i*robot_kdl_->dof+k]=gradient[k];
    }
  }
  return retval;
}
#if SPARSE_GRADIENT==true
std::vector<std::pair<vector<double>::size_type, vector<double>::size_type>> optProblem::gradient_sparsity() const
{
  std::vector<std::pair<vector<double>::size_type, vector<double>::size_type>> retval;
  // Objective gradient is dense
  for (int i=0;i<m_dim;++i)
  {
    retval.emplace_back(0,i);
  }
  // inequality constraints:
  // Velocity constraints:
  for(int i=0;i<robot_kdl_->dof;++i)
  {
    retval.emplace_back(1+m_nec+i,i);
  }
  // Other timesteps:
  for(int i=1;i<timesteps_;++i)
  {
    for(int j=0;j<robot_kdl_->dof;++j)
    {
      retval.emplace_back(1+m_nec+1*robot_kdl_->dof+(i-1)*robot_kdl_->dof+j, (i-1)*robot_kdl_->dof+j);
      retval.emplace_back(1+m_nec+1*robot_kdl_->dof+(i-1)*robot_kdl_->dof+j,(i)*robot_kdl_->dof+j);
    }
  }

  // collision constraint:
  for(int i=0;i<timesteps_;++i)
  {
    for(int j=0;j<robot_kdl_->dof;++j)
    {
      retval.emplace_back(1+m_nec+robot_kdl_->dof*timesteps_+i,i*robot_kdl_->dof+j);
    }
  }
  return retval;
}
vector<double> optProblem::sparse_gradient(const vector<double> x) const
{
  vector<double> grad;
  vector<double> obj_grad=objGradient(x);
  //vector<double> eq_grad=eqGradient(x);
  vector<double> ineq_grad=inEqGradient(x);
  vector<double> collision_grad=collisionGradient(x);
  grad.resize(m_dim,0.0);

  for(int i=0;i<m_dim;i++)
  {
    grad[i]=obj_grad[i];
  }

  // velocity constraints
  for(int j=0;j<robot_kdl_->dof;++j)
  {
    grad.emplace_back(ineq_grad[j]);
  }
  
  for(int i=1;i<timesteps_;++i)
  {
    for(int j=0;j<robot_kdl_->dof;++j)
    {
      grad.emplace_back(ineq_grad[1*robot_kdl_->dof+2*(i-1)*robot_kdl_->dof+j]);
      grad.emplace_back(ineq_grad[1*robot_kdl_->dof+2*(i-1)*robot_kdl_->dof+robot_kdl_->dof+j]);
    
    }
  }
  // collision constraint:
  for(int i=0;i<timesteps_;++i)
  {
    for(int j=0;j<robot_kdl_->dof;++j)
    {
      grad.emplace_back(collision_grad[i*robot_kdl_->dof+j]);
    }
  }

  return grad;
}
#endif
