#ifndef SPARSE_GRADIENT
#define SPARSE_GRADIENT true
#endif
#include "ll4ma_planner/problems/palm_pose_problem.hpp"
using namespace opt_problems;
optProblem::optProblem(int dummy)
{
  std::cerr<<"optProblem dummy constructor is initialized, fix me!!!"<<std::endl;
}
optProblem::optProblem(int time_steps, manipulator_kdl::robotKDL &arm_kdl, manipulator_kdl::robotKDL &arm_hand_kdl, graspEnv &c_check, Eigen::VectorXd des_pose, vector<double> vel_limits,vector<double> init_joint_angles, vector<double> hand_joints)
{
  m_dim=arm_kdl.dof*time_steps;//+arm_hand_kdl.dof;
  m_nec=0;//
  m_nic=arm_kdl.dof*(time_steps);//+time_steps;// velocity constraints, collision constraints

  timesteps_=time_steps;
  arm_kdl_=&arm_kdl;
  hand_kdl_=&arm_hand_kdl;
  des_pose_=des_pose;
  joints_0=init_joint_angles;
  vel_lim=vel_limits[0];
  Map<Eigen::VectorXd> hand_j0(&hand_joints[0],hand_joints.size());
  hand_js=hand_j0;
  
  c_checker=c_check;
  if(timesteps_>1)
  {
    wa=0.01;
  }
  else
  {
    wa=0.0;
  }
  wd=10.0;// desired palm pose
  wm=0.001;
  j_thresh=0.15;
}

double optProblem::objFunction(const vector<double> x) const
{
  // Cost function is on reaching a desired cartesian pose
  //double wc=10.0;
  // Cost function is on reaching a desired cartesian pose 
  double cost_val=0.0;
  Eigen::VectorXd q;

  Eigen::VectorXd acc_cost(arm_kdl_->dof);
  
  q.resize(arm_kdl_->dof);

  // Store joints for acceleration cost:
  vector<Eigen::VectorXd> q_arr;
  Eigen::VectorXd j0(joints_0.size());
  
  for(int i=0;i<joints_0.size();++i)
  {
    j0[i]=joints_0[i];
  }
  q_arr.push_back(j0);
  q_arr.push_back(j0);
  
  for(int i=0; i<timesteps_;++i)
  {
    for(int j=0; j<arm_kdl_->dof;++j)
    {
      q[j]=x[(i)*arm_kdl_->dof+j];
    }
    q_arr.push_back(q);
  }
  q_arr.push_back(q);
  q_arr.push_back(q);


  // Palm pose cost for final timestep:
  Eigen::VectorXd palm_pose;
  bool rpy_=true;
  arm_kdl_->getFK(0,q,palm_pose,rpy_);

  // position error:
  Eigen::VectorXd pose_err(6);
  pose_err.head(3)=des_pose_.head(3)-palm_pose.head(3);

  // orientation error:
        
  vector<double> o_d(des_pose_.data()+3, des_pose_.data()+6);
  vector<double> o_1(palm_pose.data()+3, palm_pose.data()+6);
  vector<double> o_err;
  
  arm_kdl_->euler_diff(o_1,o_d,o_err);
  pose_err[3]=o_err[0]*0.1;
  pose_err[4]=o_err[1]*0.1;
  pose_err[5]=o_err[2]*0.1;
    
  cost_val+=wd*pose_err.squaredNorm();

  for(int i=0; i<timesteps_;++i)
  {
    // min acc cost:
    acc_cost=q_arr[i-2+2]-2.0*q_arr[i+2-1]+q_arr[i+2];
    cost_val+=wa*acc_cost.squaredNorm();
    for(int j=1;j<5;j++)
    {
      cost_val+=wm*(exp(max(std::abs(q[j]),arm_kdl_->up_bounds[j+4]-j_thresh)) -exp(arm_kdl_->up_bounds[j+4]-j_thresh));
    }

  }
  return cost_val;
}
vector<double> optProblem::objGradient(vector<double> x) const
{
  vector<double> retval(m_dim,0.0);

  Eigen::VectorXd gradient,acc_cost,acc_grad,q;

  gradient.resize(7);
  gradient.setZero();
  q.resize(arm_kdl_->dof);

  vector<Eigen::VectorXd> q_arr;
  Eigen::VectorXd j0(joints_0.size());
  for(int i=0;i<joints_0.size();++i)
  {
    j0[i]=joints_0[i];
  }

  q_arr.push_back(j0);
  q_arr.push_back(j0);
  

  for(int i=0; i<timesteps_;++i)
  {
    for(int j=0; j<arm_kdl_->dof;++j)
    {
      q[j]=x[(i)*arm_kdl_->dof+j];
    }
    q_arr.push_back(q);
  }
  q_arr.push_back(q);
  q_arr.push_back(q);

  // Gradient for reaching desired pose:
  Eigen::VectorXd palm_pose;
  bool rpy_=true;
  arm_kdl_->getFK(0,q,palm_pose,rpy_);

  // position error:
  Eigen::VectorXd pose_err(6);
  pose_err.head(3)=des_pose_.head(3)-palm_pose.head(3);

  // orientation error:
        
  vector<double> o_d(des_pose_.data()+3, des_pose_.data()+6);
  vector<double> o_1(palm_pose.data()+3, palm_pose.data()+6);
  vector<double> o_err;
  
  arm_kdl_->euler_diff(o_1,o_d,o_err);
  pose_err[3]=o_err[0]*0.1;
  pose_err[4]=o_err[1]*0.1;
  pose_err[5]=o_err[2]*0.1;
  Eigen::MatrixXd J;
  arm_kdl_->getJacobian(0,q,J);

  //
  gradient=-2.0*wd*pose_err.transpose()*J;
  for(int j=0;j<arm_kdl_->dof;++j)
  {
    retval[(timesteps_-1)*arm_kdl_->dof+j]=gradient[j];
  }
  
  for(int i=0;i<timesteps_;++i)
  {
    q=q_arr[i+2];
    gradient.setZero();
    // min acc grad:
    
    
    acc_cost=wa*(q_arr[i-2+2]-2.0*q_arr[i+2-1]+q_arr[i+2]);
    
    
    acc_grad=2.0*acc_cost;
    for (int j=0;j<arm_kdl_->dof;++j)
    {
      retval[j+arm_kdl_->dof*(i)]+=acc_grad[j];
    }

    if(i>1)
    {
      acc_grad=2.0*acc_cost;
      for (int j=0;j<arm_kdl_->dof;++j)
      {
        retval[j+arm_kdl_->dof*(i-2)]+=acc_grad[j];
      } 
    }
    if(i>0)
    {
      acc_grad=-4.0*acc_cost;
      for (int j=0;j<arm_kdl_->dof;++j)
      {
        retval[j+arm_kdl_->dof*(i-1)]+=acc_grad[j];
      }
     
    }
    // mid point gradient:
    for(int j=1;j<5;j++)
    {
      retval[j+arm_kdl_->dof*(i)]+=wm*q[j]*(exp(max(std::abs(q[j]),arm_kdl_->up_bounds[j+4]-j_thresh))-exp(arm_kdl_->up_bounds[j+4]-j_thresh));
    }

    
  }
  
  return retval;
}

vector<double> optProblem::inEqConstraints(vector<double> x) const
{
  vector<double> vel_const(m_nic,0.0);

  // Encoding initial joint angle constraint as velocity constraint
  for(int j=0;j<arm_kdl_->dof;++j)
  {
    vel_const[(0)*arm_kdl_->dof+j]=std::abs(x[(0)*arm_kdl_->dof+j]-joints_0[j])-vel_lim;
  }

  for (int i=1; i<timesteps_; ++i)
  {
    for(int j=0;j<arm_kdl_->dof;++j)
    {
      vel_const[(i)*arm_kdl_->dof+j]=std::abs(x[(i)*arm_kdl_->dof+j]-x[(i-1)*arm_kdl_->dof+j])-vel_lim;
    }
  }

  return vel_const;
}
vector<double> optProblem::inEqGradient(vector<double> x) const
{
  
  vector<double> retval(1*arm_kdl_->dof+2*(arm_kdl_->dof)*(timesteps_-1),0.0);

  
  for(int j=0;j<arm_kdl_->dof;++j)
  {
    if(x[j]-joints_0[j]<0.0)
    {
      retval[j]=-1.0;
    }
    else
    {
      retval[j]=1.0;
    }
  }
  

  for(int i=1;i<timesteps_;++i)
  {
      for(int j=0;j<arm_kdl_->dof;++j)
      {
        if(x[(i)*arm_kdl_->dof+j]-x[(i-1)*arm_kdl_->dof+j]<0.0)
        {
          retval[1*arm_kdl_->dof+2*(i-1)*arm_kdl_->dof+j]=1.0;
          
          retval[1*arm_kdl_->dof+2*(i-1)*arm_kdl_->dof+arm_kdl_->dof+j]=-1.0;

        }
        else
        {
          retval[1*arm_kdl_->dof+2*(i-1)*arm_kdl_->dof+j]=-1.0;
                                                                
          retval[1*arm_kdl_->dof+2*(i-1)*arm_kdl_->dof+arm_kdl_->dof+j]=1.0;
          
        }
      }
  }
  return retval;
}
vector<double> optProblem::collisionGradient(vector<double> x) const
{
  vector<double> retval(arm_kdl_->dof*timesteps_,0.0);
  return retval;
}

vector<double> optProblem::EqConstraints(vector<double> x) const
{
  // no constraint
  vector<double> ret(m_nec,0.0);
  
  return  ret;  
}

vector<double> optProblem::eqGradient(vector<double> x) const
{
  vector<double> retval(m_dim*m_nec,0.0);
  
  return retval;
}

vector<double> optProblem::gradient(const vector<double> x) const
{
  vector<double> grad;
  vector<double> obj_grad=objGradient(x);
  vector<double> eq_grad=eqGradient(x);
  vector<double> ineq_grad=inEqGradient(x);
  
  grad.resize(m_dim+m_dim*m_nec+m_dim*m_nic,0.0);
  for(int i=0;i<m_dim;i++)
  {
    grad[i]=obj_grad[i];

  }

  for(int i=m_dim;i<m_dim+m_dim*m_nec;i++)
  {
    grad[i]=eq_grad[i-m_dim];
  }


  for(int i=m_dim+m_dim*m_nec;i<m_dim+m_dim*m_nec+m_dim*m_nic;i++)
  {
    
    grad[i]=ineq_grad[i-m_dim-m_dim*m_nec];
  }

  return grad;
}
vector<vector<double>> optProblem::bounds() const
{
  vector<vector<double>> bound;
  vector<double> up_bounds;
  vector<double> low_bounds;
  
  // Getting joint angle limits from urdf
  // currently we hack it to get only the first seven joints
  for(int i=0;i<timesteps_;++i)
  {
    up_bounds.insert(up_bounds.end(), next(arm_kdl_->up_bounds.begin(),4), next(arm_kdl_->up_bounds.begin(),arm_kdl_->dof+4));
    low_bounds.insert(low_bounds.end(), next(arm_kdl_->low_bounds.begin(),4), next(arm_kdl_->low_bounds.begin(), arm_kdl_->dof+4));
  }

  bound.resize(2);
  bound[0]=low_bounds;
  bound[1]=up_bounds;
  return bound;
  
}

#if SPARSE_GRADIENT==true
std::vector<std::pair<vector<double>::size_type, vector<double>::size_type>> optProblem::gradient_sparsity() const
{
  std::vector<std::pair<vector<double>::size_type, vector<double>::size_type>> retval;
  // Objective gradient is dense
  for (int i=0;i<m_dim;++i)
  {
    retval.emplace_back(0,i);
  }
  // inequality constraints:
  for(int i=0;i<arm_kdl_->dof;++i)
  {
    retval.emplace_back(1+m_nec+i,i);
  }
  
  // Other timesteps:
  //for(int i=0;
  for(int i=1;i<timesteps_;++i)
  {
    for(int j=0;j<arm_kdl_->dof;++j)
    {
      retval.emplace_back(1+m_nec+1*arm_kdl_->dof+(i-1)*arm_kdl_->dof+j, (i-1)*arm_kdl_->dof+j);
      retval.emplace_back(1+m_nec+1*arm_kdl_->dof+(i-1)*arm_kdl_->dof+j,(i)*arm_kdl_->dof+j);
    }
  }
  
  
  return retval;
}
vector<double> optProblem::sparse_gradient(const vector<double> x) const
{
  vector<double> grad;
  vector<double> obj_grad=objGradient(x);
  //vector<double> eq_grad=eqGradient(x);
  vector<double> ineq_grad=inEqGradient(x);
  //vector<double> collision_grad=collisionGradient(x);  
  grad.resize(m_dim,0.0);

  for(int i=0;i<m_dim;i++)
  {
    grad[i]=obj_grad[i];
  }

  for(int j=0;j<arm_kdl_->dof;++j)
  {
    grad.emplace_back(ineq_grad[j]);
  }
  
  for(int i=1;i<timesteps_;++i)
  {
    for(int j=0;j<arm_kdl_->dof;++j)
    {
      grad.emplace_back(ineq_grad[1*arm_kdl_->dof+2*(i-1)*arm_kdl_->dof+j]);
      grad.emplace_back(ineq_grad[1*arm_kdl_->dof+2*(i-1)*arm_kdl_->dof+arm_kdl_->dof+j]);
    }
  }
  
  
  return grad;
}

#endif
