#define COLLISION_CHECKING true
#define IK_PLANNER true
#include "ll4ma_planner/problems/collision_opt_problem.hpp"
#include "ll4ma_planner/pagmo_opt/trajectory_optimization.hpp"

// Robot kdl:
#include <ros/ros.h>
#include <ros/package.h>

// ROS service:
#include "ll4ma_planner/IKJointTrajectory.h"
#include <ll4ma_collision_wrapper/UpdateEnvCloud.h>
// joint trajectory
#include <trajectory_msgs/JointTrajectory.h>

using namespace pagmo;

using namespace fcl_ccheck;
class srvClass
{
public:
  robEnv c_check;
  ros::NodeHandle* nh;
  ros::Publisher pub,m_pub;
  void init(ros::NodeHandle &n)
  {
    nh=&n;
    int n_links=8;
    float voxel_filter_size=0.02;
    //float hacd_vsize=0.1;
    vector<float> min_ws_bound={-0.8,-1.2,-0.1};
    vector<float> max_ws_bound={0.8,-0.4,1.2};
    
    //cerr<<"initializing rob_env_scene"<<endl;
    robEnv rob_env_checker(n_links, voxel_filter_size,min_ws_bound,max_ws_bound);
    c_check=rob_env_checker;
    //cerr<<"copied rob_env_scene"<<endl;
  }
  void init_robot_meshes()
  {

    vector<string> link_names={"lbr4_0_link", "lbr4_1_link", "lbr4_2_link", "lbr4_3_link", "lbr4_4_link", "lbr4_5_link", "lbr4_6_link","lbr4_7_link"};
    
    // initialize collision environment:
    std::string robot_desc_string;
    
    // read urdf as a string:
    nh->param("robot_description", robot_desc_string, std::string());
    c_check.load_robot_links(link_names,robot_desc_string);
    
      
    cerr<<"Created robot collision files"<<endl;
 
  }
  bool update_env_cloud(ll4ma_collision_wrapper::UpdateEnvCloud::Request &req,ll4ma_collision_wrapper::UpdateEnvCloud::Response &res)
  {
    pub=nh->advertise<pcl::PointCloud<PointXYZ>>("/collision_checker/env",1);
    //m_pub=nh->advertise<visualization_msgs::MarkerArray>("collision_env", 1);
    ros::Rate l(100);
    cerr<<"Updating environment cloud"<<endl;
    //convert sensor_msgs to normal pointcloud
    PointCloud<PointXYZ>::Ptr e_cloud(new PointCloud<PointXYZ>);
    // Update env_cloud:
    pcl::fromROSMsg(req.env_cloud,*e_cloud);

    c_check.update_env(e_cloud,req.transform_mat);

    //visualization_msgs::MarkerArray oc_map;
    //c_check.get_env_markers(oc_map,"base_link");
    
    // visualize pointcloud:
    e_cloud->header.frame_id="base_link";
    for(int i=0;i<100;++i)
    {
      pub.publish(e_cloud);
      // visualize octomap:
      //m_pub.publish(oc_map);
      l.sleep();
    }

    return true;
  }
  
  bool ik_call(ll4ma_planner::IKJointTrajectory::Request &req,ll4ma_planner::IKJointTrajectory::Response &res)
  {
    

    
    // robot kdl
    // urdf file location:
    vector<string> ee_names={"lbr4_7_link"};
    vector<string> base_names={"base_link"};
    vector<double> g_vec={0.0,0.0,-9.8};
    
    vector<string> link_names={"lbr4_0_link","lbr4_1_link","lbr4_2_link","lbr4_3_link",
                             "lbr4_4_link","lbr4_5_link","lbr4_6_link","lbr4_7_link"};
    
    while(!nh->hasParam("robot_description"))
    {
      //loop_rate.sleep();
    }

    // initialize kdl class:
    //std::cerr<<"Building KDL model"<<std::endl;
    //robEnv r_check=c_check;
    manipulator_kdl::robotKDL lbr4_("robot_description",*nh,base_names,ee_names,link_names,g_vec);
    // Get joint limits from urdf parser:
    lbr4_.urdfParser();
    //std::cerr<<"Built KDL model"<<std::endl;
  


    // Desired parameters:
    vector<double> des_pose;
    des_pose.resize(6);    
    des_pose[0]=req.desired_pose[0];
    des_pose[1]=req.desired_pose[1];
    des_pose[2]=req.desired_pose[2];
    des_pose[3]=req.desired_pose[3];
    des_pose[4]=req.desired_pose[4];
    des_pose[5]=req.desired_pose[5];

    vector<double> joints_0;
    joints_0.resize(7);
    for(int i=0;i<req.initial_joint_angles.size();++i)
    {
      joints_0[i]=req.initial_joint_angles[i];
    }
    int t_steps=req.t_steps;      
    vector<double> vel_limits;
    if(t_steps>1)
    {
      vel_limits.resize(7,0.8);
    }
    else
    {
      vel_limits.resize(7,100.0);
    }
    //std::cerr<<"Initializing opt"<<std::endl;
    
    // Create a opt problem instance

    opt_problems::optProblem opt_problem(t_steps,lbr4_,c_check,des_pose,vel_limits,joints_0);
    //std::cerr<<"Prob initialized"<<std::endl;
    
    
    problem prob{trajectory_optimization{opt_problem}};
    vector<double> tol(7*(t_steps)+t_steps,1e-3);
    
    prob.set_c_tol(tol);
    
    pagmo::snopt7 sn_v(false,"/usr/local/lib/");
    sn_v.set_integer_option("Iterations limit",1000);
    sn_v.set_numeric_option("Major optimality tolerance",req.obj_tol);
    //sn_v.set_numeric_option("Minor feasibility tolerance",req.obj_tol);
    
    algorithm algo{sn_v};
    algo.set_verbosity(1000);
    //std::cerr<<"Creating population"<<std::endl;
    
    population pop{prob,3u};
    
    
    if(req.final_joint_angles.size()>0)
    {
      std::cerr<<"Computing initial fitness from ik: "<<std::endl;
      vector<double> x_in(7*t_steps,0.0);
      for(int i=0;i<t_steps;++i)
      {
        for(int j=0;j<lbr4_.dof;++j)
        {
          x_in[i*lbr4_.dof+j]=joints_0[j]+(req.final_joint_angles[j]-joints_0[j])*double(i+1)/double(t_steps);
        }
      }
      vector<double> f_in=prob.fitness(x_in);
      cerr<<"initial fitness cost: "<<f_in[0]<<endl;
      pop.set_xf(0,x_in,f_in);
    }
    else if(req.initial_joint_traj.points.size()>0)
    {
      std::cerr<<"Computing initial fitness from ik traj: "<<req.initial_joint_traj.points.size()<<" "<<t_steps<<std::endl;
      vector<double> x_in(7*t_steps,0.0);
      for(int i=0;i<t_steps;++i)
      {
        for(int j=0;j<lbr4_.dof;++j)
        {
          x_in[i*lbr4_.dof+j]=req.initial_joint_traj.points[i+1].positions[j];
        }
      }
      vector<double> f_in=prob.fitness(x_in);
      cerr<<"initial fitness cost: "<<f_in[0]<<endl;
      pop.set_xf(0,x_in,f_in);

    }
    
    
    //std::cerr<<"Runnnig snopt"<<std::endl;
    population pop_res=algo.evolve(pop);
    
    //cerr<<prob<<endl;
    std::vector<double> final_pose;
    cerr<<"Final cost:"<<pop_res.champion_f()[0]<<endl;
    
    std::vector<double> x_best;
    std::vector<double> final_joint;
    
    x_best.resize(7,0.0);
    final_joint.resize(7,0.0);
    x_best=pop_res.champion_x();
    
    trajectory_msgs::JointTrajectory joint_traj;
    std::vector<std::string> joint_names={"lbr4_j0","lbr4_j1","lbr4_j2","lbr4_j3","lbr4_j4","lbr4_j5","lbr4_j6"};
    joint_traj.joint_names=joint_names;
    std::vector<double> joint_angles;
    joint_angles.resize(7,0.0);

    trajectory_msgs::JointTrajectoryPoint jt_pt0;
    jt_pt0.positions=joints_0;
    joint_traj.points.push_back(jt_pt0);
    
    
    for (int i=0;i<x_best.size()/7;i++)
    { 
      trajectory_msgs::JointTrajectoryPoint jt_pt;
      for(int j=0;j<7;j++)
      {
        joint_angles[j]=x_best[i*7+j];
      }
      jt_pt.positions=joint_angles;
      joint_traj.points.push_back(jt_pt);
    }
    res.collision_free=true;
    res.joint_traj=joint_traj;
    res.champion_f=pop_res.champion_f();
    
    return true;
  }
};

using namespace std;
int main(int argc, char** argv)
{

  //collisionChecker c_check;
  ros::init(argc,argv,"lbr4_ik_collision_node");
  ros::NodeHandle n;
  srvClass srv_call;
  srv_call.init(n);
  srv_call.init_robot_meshes();
  // create ros service:
  ros::ServiceServer env_service=n.advertiseService( "/ll4ma_planner/update_env", &srvClass::update_env_cloud, &srv_call);

  ros::ServiceServer service=n.advertiseService("/ll4ma_planner/get_collision_plan",&srvClass::ik_call,&srv_call);
  ROS_INFO("LBR4 IK collision-free planner service ready");
  ros::spin();
  //return 0;
}
                                               
 
