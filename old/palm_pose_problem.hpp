#ifndef SPARSE_GRADIENT
#define SPARSE_GRADIENT true
#endif
#include "ll4ma_planner/debug.h"
#include <iostream>


#include <ll4ma_kdl/manipulator_kdl/robot_kdl.h>
//#include <ll4ma_collision_wrapper/scenes/grasp.h>
#include <ll4ma_collision_wrapper/libs/fcl/grasp.h>
// Eigen library for vectors and matrices:
#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <vector>
using namespace fcl_ccheck;
using namespace std;
namespace opt_problems
{
  class optProblem
  {
  public:
    // constructor
    optProblem(int dummy);// dummy since pagmo requires default values for variables

    optProblem(int time_steps, manipulator_kdl::robotKDL &arm_kdl, manipulator_kdl::robotKDL &arm_hand_kdl, graspEnv &c_check, Eigen::VectorXd des_pose, vector<double> vel_limits,vector<double> init_joint_angles, vector<double> hand_j0);

    // functions
    double objFunction(vector<double> x) const; 
    vector<double> inEqConstraints(vector<double> x) const;
    vector<double> EqConstraints(vector<double> x) const;
    //#if SPARSE_GRADIENT==false
    
    vector<double> gradient(vector<double> x) const;
    //#endif
    vector<double> objGradient(vector<double> x) const;
    vector<double> inEqGradient(vector<double> x) const;
    vector<double> eqGradient(vector<double> x) const;
    vector<double> collisionGradient(vector<double> x) const;
    vector<vector<double>> bounds() const;
    #if SPARSE_GRADIENT==true
    vector<double> sparse_gradient(vector<double> x) const;
    std::vector<std::pair<vector<double>::size_type, vector<double>::size_type>> gradient_sparsity() const;
    #endif
    int m_dim,m_nec,m_nic;
    vector<double> joints_0;
  private:
    manipulator_kdl::robotKDL* arm_kdl_;
    manipulator_kdl::robotKDL* hand_kdl_;
    vector<int> f_idx_;
    vector<Eigen::VectorXd> des_cpts_;

    int timesteps_;
    Eigen::VectorXd des_pose_;
    Eigen::VectorXd hand_js;
    double vel_lim;
    
    // Collision checking is a mutable object since libccd is not static/const.
    mutable graspEnv c_checker;
    double wa,wd,wm,j_thresh;

  };
}
