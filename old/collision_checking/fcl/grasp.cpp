#include "libs/fcl/grasp.h"
// constructor:

namespace fcl_ccheck
{
graspEnv::graspEnv()
{
}
graspEnv::graspEnv(int n_links, float downsample_size, vector<float> min_bound, vector<float> max_bound,float octree_resolution)
{
  n_links_=n_links;
  voxel_size_=downsample_size;
  link_objs_.resize(n_links_);
  ws_min_bound_=min_bound;
  ws_max_bound_=max_bound;
  octree_resolution_=octree_resolution;
  
  octomap::OcTree* tree = new octomap::OcTree(octree_resolution_);
  env_tree_=tree;  

  octomap::OcTree* o_tree = new octomap::OcTree(octree_resolution_);
  obj_tree_=o_tree;  

}


void graspEnv::load_robot_links(vector<string> &f_names, string &urdf_text)
{
  readLinkShapes(urdf_text, f_names, link_objs_,link_offsets_);

}
void graspEnv::load_hand_links(vector<string> &f_names, string &urdf_text)
{
  readLinkShapes(urdf_text, f_names, hand_link_objs_,hand_link_offsets_);

  n_hand_links_=f_names.size();
}

void graspEnv::update_robot_state(vector<vector<double>> &link_poses)
{
  // add offsets:
  vector<Transform3d> l_poses;
  l_poses.resize(link_poses.size());
  // TODO: this is making the computations slower, find a better way!!!
  for(int i=0;i<link_poses.size();++i)
  {
    Quaterniond q(link_poses[i][6],link_poses[i][3],link_poses[i][4],link_poses[i][5]);// fcl quaternion(w,x,y,z)
    Vector3d p(link_poses[i][0],link_poses[i][1],link_poses[i][2]);
    l_poses[i].setIdentity();
    l_poses[i].translate(p);
    l_poses[i].rotate(q);

    l_poses[i]=l_poses[i]*link_offsets_[i];
  }

  update_pose(link_objs_,l_poses);
}

void graspEnv::update_hand_state(vector<vector<double>> &link_poses)
{
  vector<Transform3d> l_poses;
  l_poses.resize(link_poses.size());
  // TODO: this is making the computations slower, find a better way!!!
  for(int i=0;i<link_poses.size();++i)
  {
    Quaterniond q(link_poses[i][6],link_poses[i][3],link_poses[i][4],link_poses[i][5]);// fcl quaternion(w,x,y,z)
    Vector3d p(link_poses[i][0],link_poses[i][1],link_poses[i][2]);
    l_poses[i].setIdentity();
    l_poses[i].translate(p);
    l_poses[i].rotate(q);

    l_poses[i]=l_poses[i]*hand_link_offsets_[i];
  }

  update_pose(hand_link_objs_,l_poses);
}

void graspEnv::update_env(PointCloud<PointXYZ>::Ptr in_cloud, geometry_msgs::Transform &transform_mat)
{
  // remove nans:
  _pcl_eigen.removenans(in_cloud);

  // Transforming the pointcloud to the robot base frame:
  _pcl_eigen.transform_pointcloud(in_cloud,transform_mat);
  
  // processing pointcloud
  
  // Downsample environment cloud:

  #ifdef DEBUG
  cerr<<"Downsampling cloud using voxel grid filter with leaf size: "<<voxel_size_<<endl;
  #endif
  _pcl_eigen.downsample_cloud(in_cloud,voxel_size_);

    // Filter pointcloud:
  // Remove points not in robot workspace:
  _pcl_eigen.bound_cloud(in_cloud,ws_min_bound_,ws_max_bound_);

  _pcl_eigen.loadPCLOctree(in_cloud,env_tree_);
  
  env_objs_=loadOctreeCollObj(env_tree_);
  cout<<"Environment boxes size: "<<env_objs_.size()<<endl;

}

void graspEnv::update_world(PointCloud<PointXYZ>::Ptr env_cloud, PointCloud<PointXYZ>::Ptr obj_cloud)
{

  _pcl_eigen.loadPCLOctree(env_cloud,env_tree_);
  env_objs_=loadOctreeCollObj(env_tree_);
  cout<<"Environment boxes size: "<<env_objs_.size()<<endl;
  

  // HACD on object pointcloud:
  _pcl_eigen.loadPCLOctree(obj_cloud,obj_tree_);
  grasp_objs_=loadOctreeCollObj(obj_tree_);
  cout<<"Object boxes size: "<<grasp_objs_.size()<<endl;
  
}
void graspEnv::rob_env_ccheck(vector<vector<double>> &l_poses, vector<vector<coll_data>> &c_data)
{
  // update robot link poses:
  update_robot_state(l_poses);
  
  c_data.resize(n_links_);
  // perform collision checking:
  
  for(int i=0;i<l_poses.size();++i)
  {
    signedDistance(link_objs_[i],env_objs_,c_data[i]);
  } 
}

void graspEnv::hand_env_ccheck(vector<vector<double>> &l_poses, vector<vector<coll_data>> &c_data)
{
  // update robot link poses:
  update_hand_state(l_poses);
  
  c_data.resize(n_hand_links_);
  // perform collision checking:
  for(int i=0;i<l_poses.size();++i)
  {
    signedDistance(hand_link_objs_[i],env_objs_,c_data[i]);
  } 

}

/*
void graspEnv::grasp_contact_check(vector<vector<double>> &l_poses, vector<vector<collision_data>> &c_data,vector<int> l_idx)
{
  //cerr<<"updating hand state"<<endl;
  // update robot link poses:
  update_hand_state(l_poses);
  //cerr<<"updated hand state"<<endl;
  c_data.resize(n_hand_links_);
  
  // get contact data
  multi_threaded_contact(hand_link_objs_,obj_,l_idx,ccd,c_data);
  //single_contact(hand_link_objs_,obj_,l_idx,ccd,c_data);
}

vector<collision_data> graspEnv::contact_fn(const obj_t &link_obj, const vector<obj_t> &narrow_env_objs, const ccd_t &ccd) const
{
  vector<collision_data> c_data;
  signed_distance_arr(link_obj,narrow_env_objs,ccd,c_data);
  return c_data;
}
*/
}
