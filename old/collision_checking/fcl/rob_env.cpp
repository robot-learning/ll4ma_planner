#include "libs/fcl/rob_env.h"

namespace fcl_ccheck
{
// constructor:
robEnv::robEnv()
{
}
robEnv::robEnv(int n_links, float downsample_size, vector<float> min_bound, vector<float> max_bound,float octree_resolution)
{
  n_links_=n_links;
  voxel_size_=downsample_size;
  link_objs_.resize(n_links_);
  ws_min_bound_=min_bound;
  ws_max_bound_=max_bound;
  octree_resolution_=octree_resolution;
  octomap::OcTree* tree = new octomap::OcTree(octree_resolution_);
  env_tree_=tree;  
}


void robEnv::load_robot_links(vector<string> &link_names,string &urdf_txt)
{
  
  readLinkShapes(urdf_txt, link_names, link_objs_,link_offsets_);
}

void robEnv::update_robot_state(vector<vector<double>> &link_poses)
{
  // add mesh offsets:
  vector<Transform3d> l_poses;
  add_offsets(link_offsets_,link_poses,l_poses);
  update_pose(link_objs_, l_poses);
}

void robEnv::update_env(PointCloud<PointXYZ>::Ptr in_cloud, geometry_msgs::Transform &transform_mat)
{
  // remove nans:
  _pcl_eigen.removenans(in_cloud);

  // Transforming the pointcloud to the robot base frame:
  _pcl_eigen.transform_pointcloud(in_cloud,transform_mat);
  
  // processing pointcloud
  
  // Downsample environment cloud:

  #ifdef DEBUG
  cerr<<"Downsampling cloud using voxel grid filter with leaf size: "<<voxel_size_<<endl;
  #endif
  _pcl_eigen.downsample_cloud(in_cloud,voxel_size_);
  
  // Filter pointcloud:
  // Remove points not in robot workspace:
  _pcl_eigen.bound_cloud(in_cloud,ws_min_bound_,ws_max_bound_);

  // create octree from pointcloud:
  _pcl_eigen.loadPCLOctree(in_cloud,env_tree_);
  // create collision boxes from octree:
  env_objs_=loadOctreeCollObj(env_tree_);
  cout<<"Environment boxes size: "<<env_objs_.size()<<endl;
}
void robEnv::rob_env_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data)
{
  // update robot link poses:
  update_robot_state(l_poses);

  // check for collisions:
  c_data.resize(l_poses.size());
  
  for(int i=0;i<l_poses.size();++i)
  {
    signedDistance(link_objs_[i],env_objs_,c_data[i]);
  }
}
void robEnv::rob_env_ccheck_multi(vector<vector<double>> &l_poses, vector<coll_data> &c_data)
{
  // update robot link poses:
  update_robot_state(l_poses);

  // check for collisions:
  c_data.resize(l_poses.size());

  vector<std::future<coll_data>> futures;
  
  for( int i=0;i<n_links_;++i)
  {
    futures.push_back(std::async(launch::async, &robEnv::multi_thrd_fn, this, link_objs_[i], env_objs_));
  }
  
  for (int i=0;i<n_links_;++i)
  {
    c_data[i]=futures[i].get();
  }
  
}

coll_data robEnv::multi_thrd_fn(CollisionObjectd* link_obj,vector<CollisionObjectd*> env)
{
  coll_data c_dat;
  signedDistance(link_obj,env,c_dat);
  return c_dat;
}

void robEnv::get_env_markers(visualization_msgs::MarkerArray &box_m, const string &frame_id)
{
  // 
  _pcl_eigen.get_marker_octree(env_tree_,box_m,frame_id);
  /*
  cerr<<"getting markers"<<endl;
  //octomap::OcTree temp_tree=*env_tree_;

  typedef double S;
  OcTree<S>* oc_tree = new OcTree<S>(std::shared_ptr<const octomap::OcTree>(env_tree_));
  cerr<<"converted to fcl octree"<<endl;
  std::vector<std::array<S, 6> > boxes = oc_tree->toBoxes();
  cerr<<"generated boxes"<<endl;
  cerr<<"size: "<<boxes.size()<<endl;

  box_m.markers.resize(boxes.size());

  
  std_msgs::ColorRGBA m_color;

  m_color.r = 0.0;
  m_color.g = 1.0;
  m_color.b = 0.0;
  m_color.a = 1.0;
  uint32_t shape = visualization_msgs::Marker::CUBE;
  for(int i=0;i<boxes.size();++i)
  {
    box_m.markers[i].header.frame_id = frame_id;
    box_m.markers[i].header.stamp = ros::Time();
    box_m.markers[i].ns = "collision_env";
    box_m.markers[i].id = i;
    box_m.markers[i].type = shape;
    
    box_m.markers[i].scale.x = boxes[i][3];
    box_m.markers[i].scale.y = boxes[i][3];
    box_m.markers[i].scale.z = boxes[i][3];
    box_m.markers[i].pose.position.x=boxes[i][0];
    box_m.markers[i].pose.position.y=boxes[i][1];
    box_m.markers[i].pose.position.z=boxes[i][2];
    

    box_m.markers[i].pose.orientation.w=1.0;

    box_m.markers[i].color = m_color;

    box_m.markers[i].action = visualization_msgs::Marker::ADD;
 
  }
  */
}
void robEnv::get_link_markers(visualization_msgs::MarkerArray &link_markers)
{
  // collision geometry does not contain the object information:
  link_markers.markers.resize(n_links_);

  std_msgs::ColorRGBA m_color;

  m_color.r = 0.0;
  m_color.g = 0.0;
  m_color.b = 1.0;
  m_color.a = 1.0;
  uint32_t shape = visualization_msgs::Marker::SPHERE;

  for(int i=0;i<n_links_;++i)
  {
    const fcl::CollisionGeometry<double>* o_geom=link_objs_[i]->getCollisionGeometry();
    // create sphere marker:
    Transform3d o_pose=link_objs_[i]->getTransform();
    Quaterniond q_o(o_pose.rotation());
    Vector3d pos(o_pose.translation());
    Vector3d c=o_geom->aabb_center+pos;
    
    double r=o_geom->aabb_radius;

    //
    link_markers.markers[i].header.frame_id = "base_link";
    link_markers.markers[i].header.stamp = ros::Time();
    link_markers.markers[i].ns = "base_link";
    link_markers.markers[i].id = i;
    link_markers.markers[i].type = shape;
    
    link_markers.markers[i].scale.x = r;
    link_markers.markers[i].scale.y = r;
    link_markers.markers[i].scale.z = r;
    link_markers.markers[i].pose.position.x=c[0];
    link_markers.markers[i].pose.position.y=c[1];
    link_markers.markers[i].pose.position.z=c[2];
    
    link_markers.markers[i].pose.orientation.x=q_o.x();
    link_markers.markers[i].pose.orientation.y=q_o.y();
    link_markers.markers[i].pose.orientation.z=q_o.z();

    link_markers.markers[i].pose.orientation.w=q_o.w();

    link_markers.markers[i].color = m_color;

    //if (link_markers.markers[i].points.size() > 0)
    link_markers.markers[i].action = visualization_msgs::Marker::ADD;
    // else
    //  link_markers.markers[i].action = visualization_msgs::Marker::DELETE;

  }
}


}
