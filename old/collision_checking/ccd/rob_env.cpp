#include <ll4ma_collision_wrapper/scenes/rob_env.h>
#include <ll4ma_collision_wrapper/libs/collision_checker/support_fn.h>
// constructor:
robEnv::robEnv()
{
}
robEnv::robEnv(int n_links, float downsample_size, float narrow_vsize, float broad_vsize, vector<float> min_bound, vector<float> max_bound)
{
  n_links_=n_links;
  voxel_size_=downsample_size;
  narrow_vsize_=narrow_vsize;
  broad_vsize_=broad_vsize;
  link_objs_.resize(n_links_);
  ws_min_bound_=min_bound;
  ws_max_bound_=max_bound;
  link_meshes_.resize(n_links_);
  
  CCD_INIT(&ccd); // initialize ccd_t struct
  
  ccd.support1       = support_convex; // support function for first object
  ccd.support2       = support_convex; // support function for second object
  ccd.center1 = center; // center function for first object
  ccd.center2 = center; // center function for second object
  ccd.mpr_tolerance = 0.0001;
  ccd.max_iterations = 1000;     // maximal number of iterations
  ccd.epa_tolerance  = 0.0001;  // maximal tolerance for EPA part

}


void robEnv::load_robot_links(vector<string> &f_names)
{

  load_meshes(f_names, link_meshes_);
  create_coll_objects(link_meshes_,link_objs_);
}

void robEnv::update_robot_state(vector<vector<double>> &link_poses)
{
  
  update_poses(link_objs_,link_poses);
}

void robEnv::update_env(PointCloud<PointXYZ>::Ptr in_cloud, geometry_msgs::Transform &transform_mat)
{
  // remove nans:
  _pcl_eigen.removenans(in_cloud);

  // Transforming the pointcloud to the robot base frame:
  _pcl_eigen.transform_pointcloud(in_cloud,transform_mat);
  
  // processing pointcloud
  
  // Downsample environment cloud:

  #ifdef DEBUG
  cerr<<"Downsampling cloud using voxel grid filter with leaf size: "<<voxel_size_<<endl;
  #endif
  _pcl_eigen.downsample_cloud(in_cloud,voxel_size_);

    // Filter pointcloud:
  // Remove points not in robot workspace:
  _pcl_eigen.bound_cloud(in_cloud,ws_min_bound_,ws_max_bound_);

  // HACD:
  load_PCL(in_cloud,broad_vsize_,env_meshes_);
  create_coll_objects(env_meshes_,broad_env_objs_);
  
  load_PCL(in_cloud,narrow_vsize_,env_meshes_);
  create_coll_objects(env_meshes_,narrow_env_objs_);
  cerr<<"Broad phase size: "<<broad_env_objs_.size()<<" Narrow: "<<narrow_env_objs_.size()<<endl;  
}

void robEnv::rob_env_ccheck(vector<vector<double>> &l_poses, vector<vector<collision_data>> &c_data)
{
  // update robot link poses:
  update_robot_state(l_poses);
  
  c_data.resize(n_links_);
  // perform collision checking:
  //single_threaded(link_objs_,broad_env_objs_,narrow_env_objs_,ccd,c_data);
  multi_threaded(link_objs_,broad_env_objs_,narrow_env_objs_,ccd,c_data);
}

void robEnv::single_threaded(const vector<obj_t> &link_objs, const vector<obj_t> &broad_env_objs, const vector<obj_t> &narrow_env_objs,const ccd_t &ccd,vector<vector<collision_data>> &c_data) const
{

  bool collision=false;
  
  for( int i=0;i<n_links_;++i)
  {
    detect_collisions(link_objs[i],broad_env_objs,ccd,collision);
    if(collision==true)
    {
      signed_distance_arr(link_objs[i],narrow_env_objs,ccd,c_data[i]);
    }
    collision=false;
  }
}

void robEnv::multi_threaded(const vector<obj_t> &link_objs, const vector<obj_t> &broad_env_objs, const vector<obj_t> &narrow_env_objs,const ccd_t &ccd,vector<vector<collision_data>> &c_data) const
{
  // parallel the below loop:
  // each thread i stores: link_objs[i],broad_env_objs_, narrow_env_objs=c_data[i]
  vector<std::future<vector<collision_data>>> futures;
  
  for( int i=0;i<n_links_;++i)
  {
    futures.push_back(std::async(launch::async, &robEnv::multi_thrd_fn, this, link_objs[i], broad_env_objs,narrow_env_objs,ccd));
  }
  
  for (int i=0;i<n_links_;++i)
  {
    c_data[i]=futures[i].get();
  }
}
vector<collision_data> robEnv::multi_thrd_fn(const obj_t &link_obj, const vector<obj_t> &broad_env_objs, const vector<obj_t> &narrow_env_objs, const ccd_t &ccd) const
{
  vector<collision_data> c_data;
  /*
  bool collision=false;
  detect_collisions(link_obj,broad_env_objs,ccd,collision);
  if(collision==true)
  {
  */
  signed_distance_arr(link_obj,narrow_env_objs,ccd,c_data);
    //}
  return c_data;
}

