#include "libs/kris_library/rob_env.h"

namespace kris_ccheck
{
using namespace Meshing;
using namespace Geometry;

// constructor:
robEnv::robEnv()
{
}
robEnv::robEnv(int n_links, float downsample_size, vector<float> min_bound, vector<float> max_bound)
{
  n_links_=n_links;
  voxel_size_=downsample_size;
  link_objs_.resize(n_links_);
  ws_min_bound_=min_bound;
  ws_max_bound_=max_bound;
}


void robEnv::load_robot_links(vector<string> &link_names,string &urdf_txt)
{
  
  readLinkShapes(urdf_txt, link_names, link_objs_,link_offsets_);
}

void robEnv::update_robot_state(vector<vector<double>> &link_poses)
{
  // add mesh offsets:
  vector<Transform3d> l_poses;
  //add_offsets(link_offsets_,link_poses,l_poses);
  update_pose(link_objs_, l_poses);
}


void robEnv::add_env_meshes(vector<string> f_names, vector<Eigen::Matrix4d> &T)
{
  if(env_objs_.size()<f_names.size())
  {
    env_objs_.resize(f_names.size());
  }
  for(int i =0;i<f_names.size();i++)
  {
    env_objs_[i]=loadMeshShape(f_names[i],T[i]);
  }
}

void robEnv::update_env_poses(vector<vector<double>> &pose,vector<int> &idx)
{
  // update only idx collision objects:
  for(int i=0;i<idx.size();i++)
  {
    update_pose(env_objs_[idx[i]],pose[i]);
  }
}
void robEnv::update_env(pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloud, geometry_msgs::Transform &transform_mat)
{
  //cerr<<in_cloud->isOrganized()<<endl;
  // remove nans:
  _pcl_eigen.removenans(in_cloud);
  //cerr<<in_cloud->isOrganized()<<endl;

  // Transforming the pointcloud to the robot base frame:
  _pcl_eigen.transform_pointcloud(in_cloud,transform_mat);
  //cerr<<in_cloud->isOrganized()<<endl;
  
  // processing pointcloud
  
  // Downsample environment cloud:

  #ifdef DEBUG
  cerr<<"Downsampling cloud using voxel grid filter with leaf size: "<<voxel_size_<<endl;
  #endif
  _pcl_eigen.downsample_cloud(in_cloud,voxel_size_);
  cerr<<in_cloud->isOrganized()<<endl;
  
  // Filter pointcloud:
  // Remove points not in robot workspace:
  _pcl_eigen.bound_cloud(in_cloud,ws_min_bound_,ws_max_bound_);
  cerr<<in_cloud->isOrganized()<<endl;


  // create collision object from pointcloud:
  Eigen::Matrix4d t_mat;
  t_mat.setIdentity();
  env_objs_.resize(1);

  // write pointcloud to file
  string f_name="/tmp/pcd_env";
  pcl::io::savePCDFileASCII (f_name, *in_cloud);
  // 
  env_objs_[0]=loadPCDFile(f_name,t_mat);
  
  // create octree from pointcloud:
}

void robEnv::rob_env_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data)
{
  // update robot link poses:
  update_robot_state(l_poses);

  // check for collisions:
  c_data.resize(l_poses.size());
  
  for(int i=0;i<l_poses.size();++i)
  {
    signedDistance(link_objs_[i],env_objs_,c_data[i]);
  }
}

void robEnv::rob_env_ccheck_multi(vector<vector<double>> &l_poses, vector<coll_data> &c_data)
{
  // update robot link poses:
  update_robot_state(l_poses);

  // check for collisions:
  c_data.resize(l_poses.size());

  vector<std::future<coll_data>> futures;
  
  for( int i=0;i<n_links_;++i)
  {
    futures.push_back(std::async(launch::async, &robEnv::multi_thrd_fn, this, link_objs_[i], env_objs_));
  }
  
  for (int i=0;i<n_links_;++i)
  {
    c_data[i]=futures[i].get();
  }
  
}

coll_data robEnv::multi_thrd_fn(CollisionMesh* link_obj,vector<CollisionMesh*> env)
{
  coll_data c_dat;
  signedDistance(link_obj,env,c_dat);
  return c_dat;
}

}

