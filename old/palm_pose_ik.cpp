#include "ll4ma_planner/problems/palm_pose_problem.hpp"
#include "ll4ma_planner/pagmo_opt/trajectory_optimization.hpp"

// Robot kdl:
#include <ros/ros.h>
#include <ros/package.h>

// ROS service:
#include "ll4ma_planner/PalmPosePlan.h"
#include "ll4ma_collision_wrapper/UpdateEnvCloud.h"

// joint trajectory
#include <trajectory_msgs/JointTrajectory.h>

using namespace pagmo;

ros::Publisher pub;

class srvClass
{
public:
  graspEnv c_check;
  ros::NodeHandle* nh;
  //ros::Publisher pub;
  void init(ros::NodeHandle &n)
  {
    nh=&n;
    int n_links=8;
    float voxel_filter_size=0.02;
    //float hacd_vsize=0.1;
    float broad_vsize=1.0;
    float narrow_vsize=0.04;
    vector<float> min_ws_bound={-0.44,-1.2,-0.05};
    vector<float> max_ws_bound={0.8,-0.4,1.2};
    
    cerr<<"initializing rob_env_scene"<<endl;
    graspEnv rob_env_checker(n_links, voxel_filter_size,narrow_vsize,broad_vsize,min_ws_bound,max_ws_bound);
    c_check=rob_env_checker;
    cerr<<"copied rob_env_scene"<<endl;
  }
  void init_robot_meshes()
  {
    //c_check.min_env_size=50;
    int DOF=8;// here its the number of links= n_joints+1
    std::string mesh_location=ros::package::getPath("urlg_robots_description");
    cerr<<mesh_location<<endl;
    mesh_location.append("/meshes/lbr4/convex_hull/vertices/");
  
    // robot mesh file names:
    vector<string> r_files={"link_0.ply", "link_1.ply", "link_2.ply", "link_3.ply", "link_4.ply", "link_5.ply", "link_6.ply"};
    
    for(int i=0;i<r_files.size();++i)
    {
      r_files[i].insert(0,mesh_location);
    }
    std::string hand_mesh_location=ros::package::getPath("urlg_robots_description");
    hand_mesh_location.append("/meshes/allegro/convex_hull/vertices/");
    
    // add palm as end effector to robot:
    string ee_file="palm_link.ply";
    ee_file.insert(0,hand_mesh_location);
    r_files.push_back(ee_file);
    
    
    // initialize collision environment:
    c_check.load_robot_links(r_files);
    
    
    // load hand links:
    vector<string> hand_link_names={"index_link_1","index_link_2","index_tip", "index_link_1","index_link_2","index_tip","index_link_1","index_link_2","index_tip", "thumb_link_2","thumb_link_3","thumb_tip"};
    for(int i=0;i<hand_link_names.size();++i)
    {
      hand_link_names[i].insert(0,hand_mesh_location);
      hand_link_names[i].append(".ply");
    }
    c_check.load_hand_links(hand_link_names);

    cerr<<"Created robot collision files"<<endl;
 
  }
  bool update_env_cloud(ll4ma_collision_wrapper::UpdateEnvCloud::Request &req, ll4ma_collision_wrapper::UpdateEnvCloud::Response &res)
  {
    cerr<<"Updating environment cloud"<<endl;

    //convert sensor_msgs to normal pointcloud
    PointCloud<PointXYZ>::Ptr e_cloud(new PointCloud<PointXYZ>);
    // Update env_cloud:
    pcl::fromROSMsg(req.env_cloud,*e_cloud);

    c_check.update_env(e_cloud,req.transform_mat);
    // visualize pointcloud:
    e_cloud->header.frame_id="base_link";
    pub.publish(e_cloud);
    return true;
  }
  
  bool ik_call(ll4ma_planner::PalmPosePlan::Request &req,ll4ma_planner::PalmPosePlan::Response &res)
  {
    vector<string> ee_names={"palm_link"};
    vector<string> base_names={"base_link"};
    vector<double> g_vec={0.0,0.0,-9.8};
    
    vector<string> link_names={"lbr4_0_link","lbr4_1_link","lbr4_2_link","lbr4_3_link",
                             "lbr4_4_link","lbr4_5_link","lbr4_6_link","palm_link"};

    vector<string> hand_base_names={"base_link","base_link","base_link","base_link"};
    vector<string> hand_ee_names={"index_tip","middle_tip","ring_tip","thumb_tip"}; 
    vector<string> hand_link_names={"index_link_1","index_link_2","index_tip", "middle_link_1","middle_link_2","middle_tip", "ring_link_1","ring_link_2","ring_tip", "thumb_link_2","thumb_link_3","thumb_tip"};
    
    while(!nh->hasParam("robot_description"))
    {
      //loop_rate.sleep();
    }

    // initialize kdl class:
    //std::cerr<<"Building KDL model"<<std::endl;
    //graspEnv r_check=c_check;
    manipulator_kdl::robotKDL lbr4_("robot_description",*nh,base_names,ee_names,link_names,g_vec);
    manipulator_kdl::robotKDL lbr4_allegro_("robot_description",*nh,hand_base_names, hand_ee_names, hand_link_names,g_vec); 

    // Get joint limits from urdf parser:
    lbr4_.urdfParser();
    lbr4_allegro_.urdfParser();
    lbr4_allegro_.dof=16;
    lbr4_.dof=7;
    //std::cerr<<"Built KDL model"<<std::endl;
    
    vector<double> joints_0,arm_j0;
    joints_0.resize(lbr4_.dof+lbr4_allegro_.dof);
    arm_j0.resize(lbr4_.dof);
    
    for(int i=0;i<req.arm_jstate.position.size();++i)
    {
      joints_0[i]=req.arm_jstate.position[i];
      arm_j0[i]=req.arm_jstate.position[i];
    }

    for(int i=0;i<lbr4_allegro_.dof;++i)
    {
      joints_0[i+lbr4_.dof]=req.hand_jstate.position[i];
    }
    
    vector<double> vel_limits;
    int tim_st=req.t_steps;
    
    if(tim_st>1)
    {
      vel_limits.resize(7,0.6);
    }
    else
    {
      vel_limits.resize(7,100.0);
    }

    vector<double> hand_j0;
    Eigen::VectorXd des_pose(6);
    des_pose[0]=req.des_palm_pose.position.x;
    des_pose[1]=req.des_palm_pose.position.y;
    des_pose[2]=req.des_palm_pose.position.z;
    vector<double> orient;
    orient=lbr4_.get_RPY_q(req.des_palm_pose.orientation.x, req.des_palm_pose.orientation.y, req.des_palm_pose.orientation.z, req.des_palm_pose.orientation.w);
    des_pose[3]=orient[0];
    des_pose[4]=orient[1];
    des_pose[5]=orient[2];
    
    // Create a opt problem instance
    hand_j0=req.hand_jstate.position;
    opt_problems::optProblem opt_problem(tim_st,lbr4_,lbr4_allegro_, c_check, des_pose, vel_limits,arm_j0,hand_j0);
    
    //std::cerr<<"Prob initialized"<<std::endl;
    problem prob{trajectory_optimization{opt_problem}};
    
    vector<double> tol(opt_problem.m_nic,1e-3);
    
    prob.set_c_tol(tol);
    
    pagmo::snopt7 sn_v(false,"/usr/local/lib/");
    sn_v.set_integer_option("Verify level",-1);
    //sn_v.set_numeric_option("Iterations limit",200);
    //sn_v.set_numeric_option("Major optimality tolerance",1e-3);

    algorithm algo{sn_v};
    algo.set_verbosity(10);
    
    //std::cerr<<"Creating population"<<std::endl;
    unsigned int init=10u;
    //std::cerr<<"Computing initial fitness from initial robot state"<<std::endl;
    vector<double> x_in(7*tim_st,0.5);
    
    if(req.initial_jtraj.points.size()==0)
    {
      for(int i=0;i<tim_st;++i)
      {
        for(int j=0;j<7;++j)
        {
          x_in[i*7+j]=req.arm_jstate.position[j];
        }
      }
    
    }
    else
    {
      init=1u;
      cerr<<"initializing from traj"<<endl;
      for(int i=0;i<tim_st;++i)
      {
        for(int j=0;j<7;++j)
        {
          x_in[i*7+j]=req.initial_jtraj.points[0].positions[j]+(req.initial_jtraj.points[req.initial_jtraj.points.size()-1].positions[j]-req.initial_jtraj.points[0].positions[j])*double(i+1)/double(tim_st);
        }
      }
    
    }
    population pop{prob,init};

    vector<double> f_in=prob.fitness(x_in);
    pop.set_xf(0,x_in,f_in);
 
    
    //std::cerr<<"Runnnig snopt"<<std::endl;
    population pop_res=algo.evolve(pop);
    
    //cerr<<prob<<endl;
    std::vector<double> final_pose;
    cerr<<"Final cost:"<<pop_res.champion_f()[0]<<endl;
    
    std::vector<double> x_best;
    std::vector<double> final_joint;
    
    x_best=pop_res.champion_x();
    
    trajectory_msgs::JointTrajectory joint_traj,arm_joint_traj;
    std::vector<std::string> joint_names=req.arm_jstate.name;
    arm_joint_traj.joint_names=joint_names;
    joint_names.insert(joint_names.end(),req.hand_jstate.name.begin(),req.hand_jstate.name.end());
    joint_traj.joint_names=joint_names;
    std::vector<double> joint_angles,arm_joint_angles;
    joint_angles.resize(lbr4_.dof+lbr4_allegro_.dof,0.0);
    arm_joint_angles.resize(lbr4_.dof,0.0);
    
    trajectory_msgs::JointTrajectoryPoint jt_pt0;
    jt_pt0.positions=joints_0;
    joint_traj.points.push_back(jt_pt0);
    jt_pt0.positions=arm_j0;
    arm_joint_traj.points.push_back(jt_pt0);
    for (int i=0;i<tim_st;i++)
    { 
      trajectory_msgs::JointTrajectoryPoint jt_pt;
      for(int j=0;j<7;j++)
      {
        joint_angles[j]=x_best[i*7+j];
        arm_joint_angles[j]=x_best[i*7+j];
      }
      for(int j=0;j<lbr4_allegro_.dof;++j)
      {
        joint_angles[lbr4_.dof+j]=joints_0[lbr4_.dof+j];
      }
      
      jt_pt.positions=joint_angles;
      joint_traj.points.push_back(jt_pt);
      jt_pt.positions=arm_joint_angles;
      arm_joint_traj.points.push_back(jt_pt);
      
    }

    res.arm_joint_traj=arm_joint_traj;
    res.robot_joint_traj=joint_traj;
    res.champion_f=pop_res.champion_f();

    return true;
  }
};

using namespace std;
int main(int argc, char** argv)
{
  //collisionChecker c_check;
  ros::init(argc,argv,"grasp_planner_ik");
  ros::NodeHandle n;
  srvClass srv_call;
  srv_call.init(n);
  srv_call.init_robot_meshes();
  // create ros service:

  ros::ServiceServer service=n.advertiseService("/palm_planner/get_ik",&srvClass::ik_call,&srv_call);
  ROS_INFO("LBR4 IK collision-free planner service ready");
  ros::spin();
  //return 0;
}
                                               
 
