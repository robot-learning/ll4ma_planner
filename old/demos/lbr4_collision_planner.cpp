#define COLLISION_CHECKING true
#define IK_PLANNER true
#include "ll4ma_planner/problems/collision_opt_problem.hpp"
#include "ll4ma_planner/pagmo_opt/trajectory_optimization.hpp"

// Robot kdl:
#include <ros/ros.h>
#include <ros/package.h>

// ROS service:
#include "ll4ma_planner/IKJointTrajectory.h"
#include <ll4ma_collision_wrapper/UpdateEnvCloud.h>
// joint trajectory
#include <trajectory_msgs/JointTrajectory.h>

using namespace pagmo;

using namespace kris_ccheck;
class srvClass
{
public:
  robEnv c_check;
  ros::NodeHandle* nh;
  ros::Publisher pub,m_pub;
  string base_frame_;
  manipulator_kdl::robotKDL robot_kdl_;
  void pub_marker(string uri_file_name, vector<double> pose, string pub_topic,vector<double> color={0.8,0.8,0.8})
  {
    m_pub=nh->advertise<visualization_msgs::Marker>(pub_topic, 1);

    visualization_msgs::Marker marker;
    marker.header.frame_id = base_frame_;
    marker.ns = "basic_shapes";
    marker.id = 0;
  
    // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
  
    // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
    marker.action = visualization_msgs::Marker::ADD;
    
    // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    marker.pose.position.x = pose[0];
    marker.pose.position.y = pose[1];
    marker.pose.position.z = pose[2];
    // compute quaternion from rpy:
    tf::Quaternion q;
    q.setRPY(pose[3],pose[4],pose[5]);
    marker.pose.orientation.x = q.x();
    marker.pose.orientation.y = q.y();
    marker.pose.orientation.z = q.z();
    marker.pose.orientation.w = q.w();
    
    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker.scale.x = 1.0;
    marker.scale.y = 1;
    marker.scale.z = 1;

    // Set the color -- be sure to set alpha to something non-zero!
    marker.color.r = color[0];
    marker.color.g = color[1];
    marker.color.b = color[2];
    marker.color.a = 1.0;

    marker.lifetime = ros::Duration();
    

  // mesh marker:
    uint32_t mesh_shape= visualization_msgs::Marker::MESH_RESOURCE;
    marker.type=mesh_shape;
    marker.mesh_resource=uri_file_name;
    ros::Rate l(100);

    for(int i=0;i<100;++i)
    {
      m_pub.publish(marker);
      l.sleep();
    }

  }
  void init_env()
  {
    // load environment mesh:
    string env_mesh;
    assert(nh->getParam("env_mesh",env_mesh));
    vector<double> pose;
    assert(nh->getParam("env_pose",pose));
    
    string stl_file=ros_uri::absolute_path(env_mesh);


    string obj_mesh;
    assert(nh->getParam("obj_mesh",obj_mesh));
    assert(nh->getParam("obj_pose",obj_pose));
    string obj_stl_file=ros_uri::absolute_path(obj_mesh);
    

    vector<string> f_names={stl_file,obj_stl_file};
    
    vector<Eigen::Matrix4d> t_;
    t_.resize(2);
    t_[0].setIdentity();
    
    t_[0](0,3)=pose[0];
    t_[0](1,3)=pose[1];
    t_[0](2,3)=pose[2];

    // compute rotation matrix from euler:
    tf::Matrix3x3 R;
    R.setRPY(pose[3],pose[4],pose[5]);
    for (int i=0;i<3;++i)
    {
      t_[0](i,0)=R[i].x();
      t_[0](i,1)=R[i].y();
      t_[0](i,2)=R[i].z();

    }
    
    t_[1].setIdentity();
    
    t_[1](0,3)=obj_pose[0];
    t_[1](1,3)=obj_pose[1];
    t_[1](2,3)=obj_pose[2];

    // compute rotation matrix from euler:
    R.setRPY(obj_pose[3],obj_pose[4],obj_pose[5]);
    for (int i=0;i<3;++i)
    {
      t_[1](i,0)=R[i].x();
      t_[1](i,1)=R[i].y();
      t_[1](i,2)=R[i].z();

    }

    

    c_check.add_env_meshes(f_names,t_);
    cerr<<"added environment mesh!"<<endl;
    // publish environment as marker:
    pub_marker(env_mesh,pose,"env_marker");
  }
  void init_robot_meshes()
  {
    // read parameters from rosparam server:
    
    vector<string> arm_link_names;
    vector<string> hand_link_names;
    string urdf_param;

    int n_links;
    // default params:
    vector<string> ee_names;//={"palm_link","index_tip","middle_tip","ring_tip","thumb_tip"};
    vector<string> base_names;//={"base_link","base_link","base_link","base_link","base_link"};
    vector<string> j_names;
    assert(nh->getParam("urdf_param",urdf_param));
    
    while(!nh->hasParam(urdf_param) && ros::ok())
    {
    }
    
    // read parameters from rosparam server:
    assert(nh->getParam("base_frames",base_names));
    base_frame_=base_names[0];
    assert(nh->getParam("ee_frames",ee_names));
    assert(nh->getParam("joint_names",j_names));
    //assert(nh->getParam("arm_coll_links",arm_link_names));
    assert(nh->getParam("hand_coll_links",hand_link_names));
    assert(nh->getParam("contact_link_index", contact_links));
    // store indexing of collision links and contact links:
    // we store collision checking links first and then contact links;
    
    vector<string> robot_link_names=arm_link_names;
    robot_link_names.insert(robot_link_names.end(),hand_link_names.begin(),hand_link_names.end());
    
    //robot_link_names.insert(robot_link_names.end(),hand_contact_link_names.begin(),hand_contact_link_names.end());

    // build collision meshes:
    std::string robot_desc_string;
    
    // read urdf as a string:
    nh->param(urdf_param, robot_desc_string, std::string());
    c_check.load_robot_links(arm_link_names,hand_link_names,robot_desc_string);

    cerr<<"Created robot collision objects"<<endl;

    
    // build kdl model:
    vector<double> g_vec={0.0,0.0,-9.8};
    robot_kdl_=manipulator_kdl::robotKDL(urdf_param,*nh,base_names,ee_names,robot_link_names,g_vec);

    
    robot_kdl_.getJointLimits(j_names,robot_kdl_.up_bounds,robot_kdl_.low_bounds);
    cerr<<robot_kdl_.up_bounds.size()<<" "<<robot_kdl_.dof<<endl;
    
    for (int i=0;i<robot_kdl_.up_bounds.size();++i)
    {
      cerr<<" "<<robot_kdl_.low_bounds[i];
    }
    cerr<<"Created robot kdl object"<<endl;
 
  }

  bool ik_call(ll4ma_planner::IKJointTrajectory::Request &req,ll4ma_planner::IKJointTrajectory::Response &res)
  {
  
    // Desired parameters:
    vector<double> des_pose;
    des_pose.resize(6);    
    des_pose[0]=req.desired_pose[0];
    des_pose[1]=req.desired_pose[1];
    des_pose[2]=req.desired_pose[2];
    des_pose[3]=req.desired_pose[3];
    des_pose[4]=req.desired_pose[4];
    des_pose[5]=req.desired_pose[5];
    int T=20;

    vector<double> joints_0;
    joints_0.resize(robot_kdl_.dof);
    for(int i=0;i<req.initial_joint_angles.size();++i)
    {
      joints_0[i]=req.initial_joint_angles[i];
    }
    int t_steps=req.t_steps;      
    vector<double> vel_limits;
    if(t_steps>1)
    {
      vel_limits.resize(robot_kdl_.dof,0.4*float(T/float(t_steps)));// total time is 10 seconds
    }
    else
    {
      vel_limits.resize(robot_kdl_.dof,100.0);
    }
    //std::cerr<<"Initializing opt"<<std::endl;
    
    // Create a opt problem instance

    opt_problems::optProblem opt_problem(t_steps,robot_kdl_,c_check,des_pose,vel_limits,joints_0);
    //std::cerr<<"Prob initialized"<<std::endl;
    
    
    problem prob{trajectory_optimization{opt_problem}};
    vector<double> tol(robot_kdl_.dof*(t_steps)+t_steps,1e-6);
    
    prob.set_c_tol(tol);
    
    pagmo::snopt7 sn_v(false,"/usr/local/lib/");
    sn_v.set_integer_option("Iterations limit",5000);
    sn_v.set_numeric_option("Major optimality tolerance",req.obj_tol);
    //sn_v.set_numeric_option("Minor feasibility tolerance",req.obj_tol);
    
    algorithm algo{sn_v};
    algo.set_verbosity(100);
    
    population pop{prob,50u};
    
    
    if(req.final_joint_angles.size()>0)
    {
      std::cerr<<"Computing initial fitness from ik: "<<std::endl;
      vector<double> x_in(robot_kdl_.dof*t_steps,0.0);
      for(int i=0;i<t_steps;++i)
      {
        for(int j=0;j<robot_kdl_.dof;++j)
        {
          x_in[i*robot_kdl_.dof+j]=joints_0[j]+(req.final_joint_angles[j]-joints_0[j])*double(i+1)/double(t_steps);
        }
      }
      vector<double> f_in=prob.fitness(x_in);
      cerr<<"initial fitness cost: "<<f_in[0]<<endl;
      pop.set_xf(0,x_in,f_in);
    }
    else if(req.initial_joint_traj.points.size()>0)
    {
      std::cerr<<"Computing initial fitness from ik traj: "<<req.initial_joint_traj.points.size()<<" "<<t_steps<<std::endl;
      vector<double> x_in(robot_kdl_.dof*t_steps,0.0);
      for(int i=0;i<t_steps;++i)
      {
        for(int j=0;j<robot_kdl_.dof;++j)
        {
          x_in[i*robot_kdl_.dof+j]=req.initial_joint_traj.points[i+1].positions[j];
        }
      }
      vector<double> f_in=prob.fitness(x_in);
      cerr<<"initial fitness cost: "<<f_in[0]<<endl;
      pop.set_xf(0,x_in,f_in);

    }
    
    
    //std::cerr<<"Runnnig snopt"<<std::endl;
    population pop_res=algo.evolve(pop);
    
    //cerr<<prob<<endl;
    std::vector<double> final_pose;
    cerr<<"Final cost:"<<pop_res.champion_f()[0]<<endl;
    
    std::vector<double> x_best;
    std::vector<double> final_joint;
    
    x_best.resize(robot_kdl_.dof,0.0);
    final_joint.resize(robot_kdl_.dof,0.0);
    x_best=pop_res.champion_x();
    
    trajectory_msgs::JointTrajectory joint_traj;
    std::vector<std::string> joint_names={"lbr4_j0","lbr4_j1","lbr4_j2","lbr4_j3","lbr4_j4","lbr4_j5","lbr4_j6"};
    joint_traj.joint_names=joint_names;
    std::vector<double> joint_angles;
    joint_angles.resize(robot_kdl_.dof,0.0);

    trajectory_msgs::JointTrajectoryPoint jt_pt0;
    jt_pt0.positions=joints_0;
    joint_traj.points.push_back(jt_pt0);
    
    
    for (int i=0;i<x_best.size()/robot_kdl_.dof;i++)
    { 
      trajectory_msgs::JointTrajectoryPoint jt_pt;
      for(int j=0;j<robot_kdl_.dof;j++)
      {
        joint_angles[j]=x_best[i*robot_kdl_.dof+j];
      }
      jt_pt.positions=joint_angles;
      jt_pt.time_from_start=ros::Duration(float(i)*T/float(t_steps));
      joint_traj.points.push_back(jt_pt);
      
    }
    res.collision_free=true;
    joint_traj.header.stamp=ros::Time::now();
    res.joint_traj=joint_traj;
    res.champion_f=pop_res.champion_f();
    
    return true;
  }
};

using namespace std;
int main(int argc, char** argv)
{

  //collisionChecker c_check;
  ros::init(argc,argv,"ik_collision_node");
  ros::NodeHandle n;
  srvClass srv_call;
  srv_call.init(n);
  // create ros service:

  ros::ServiceServer service=n.advertiseService("get_safe_plan",&srvClass::ik_call,&srv_call);
  ROS_INFO("IK collision-free planner service ready");
  ros::spin();
  //return 0;
}
                                               
 
