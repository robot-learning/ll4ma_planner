#include <ll4ma_collision_wrapper/libs/fcl/rob_env.h>
#include <ros/ros.h>
#include <ros/package.h>
#include <ctime>
using namespace fcl_ccheck;
int main(int argc, char** argv)
{
  using milli = std::chrono::milliseconds;
  auto start = std::chrono::high_resolution_clock::now();
  auto finish = std::chrono::high_resolution_clock::now();
  double elapsed_secs=std::chrono::duration_cast<milli>(finish - start).count();
  double ms=1000.0;
  
  // Initialize instance of rob_env:
  // parameters for rob_env:
  vector<string> link_names={"lbr4_0_link", "lbr4_1_link", "lbr4_2_link", "lbr4_3_link", "lbr4_4_link", "lbr4_5_link", "lbr4_6_link"};
  int n_links=link_names.size();

  // environment filtering parameters:
  float voxel_filter_size=0.01;
  vector<float> min_ws_bound={-1.2,-1.2,0.1};
  vector<float> max_ws_bound={1.2,1.2,1.2};

  robEnv rob_env_checker(n_links, voxel_filter_size,min_ws_bound,max_ws_bound,0.1f);
  
  // read robot urdf:
  ros::init(argc, argv, "fcl_demo");
  
  ros::NodeHandle nh;

  std::string robot_desc_string;

  // read urdf as a string:
  nh.param("robot_description", robot_desc_string, std::string());

  start = std::chrono::high_resolution_clock::now();


  

  // load links from urdf:
  rob_env_checker.load_robot_links(link_names,robot_desc_string);
  finish = std::chrono::high_resolution_clock::now();
  elapsed_secs=std::chrono::duration_cast<milli>(finish - start).count();
  cerr<<"Loading robot meshes took:"<<elapsed_secs/ms<<endl;

  // visualize robot links:
  visualization_msgs::MarkerArray l_m;
  
  // This should from your data directory:
  string pcd_file=ros::package::getPath("ll4ma_collision_wrapper");
  pcd_file.append("/data/table.pcd");
  ROS_INFO_STREAM("Loading pcd file from location " << pcd_file);




  // load pointcloud from file:

  PointCloud<PointXYZ>::Ptr env_cloud(new PointCloud<PointXYZ>);
  start = std::chrono::high_resolution_clock::now();
  rob_env_checker._pcl_eigen.loadPCD(pcd_file,env_cloud);
  finish = std::chrono::high_resolution_clock::now();
  elapsed_secs=std::chrono::duration_cast<milli>(finish - start).count();

  cerr<<"Loading pcd took:"<<elapsed_secs/ms<<endl;

  // initialize environment:
  geometry_msgs::Transform base_frame;
  base_frame.translation.x=0.0;
  base_frame.translation.y=0.0;
  base_frame.translation.z=0.0;
  base_frame.rotation.x=0.0;
  base_frame.rotation.y=0.0;
  base_frame.rotation.z=0.0;
  base_frame.rotation.w=1.0;
  start = std::chrono::high_resolution_clock::now();

  rob_env_checker.update_env(env_cloud,base_frame);
  finish = std::chrono::high_resolution_clock::now();
  elapsed_secs=std::chrono::duration_cast<milli>(finish - start).count();

  cerr<<"Loading env took:"<<elapsed_secs/ms<<endl;
  rob_env_checker.get_env_markers(l_m,"base_link");

  vector<vector<double>> robot_link_poses;
  robot_link_poses.emplace_back(vector<double>{0.0,0.0,0.0,0.0,0.0,0.0,1.0});
  robot_link_poses.emplace_back(vector<double>{0.0,0.0,0.11,0.0,0.0,0.0,1.0});
  robot_link_poses.emplace_back(vector<double>{0.0,0.0,0.31,0.0,0.0,0.0,1.0});
  robot_link_poses.emplace_back(vector<double>{0.0,0.0,0.502,0.0,0.0,0.0,1.0});
  robot_link_poses.emplace_back(vector<double>{0.0,0.0,0.702,0.0,0.0,0.0,1.0});
  robot_link_poses.emplace_back(vector<double>{0.0,0.0,0.902,0.0,0.0,0.0,1.0});
  robot_link_poses.emplace_back(vector<double>{0.0,0.0,1.092,0.0,0.0,0.0,1.0});

  vector<coll_data> c_data;
  //rob_env_checker.update_robot_state(robot_link_poses);
  start = std::chrono::high_resolution_clock::now();

  for (int i=0;i<1500;++i)
  {
    // Get robot poses from manually:
    rob_env_checker.rob_env_ccheck(robot_link_poses,c_data);
    
  }
  finish = std::chrono::high_resolution_clock::now();
  elapsed_secs=std::chrono::duration_cast<milli>(finish - start).count();
  cerr<<"Time taken for collision checking(single): "<<elapsed_secs/ms<<endl;
    //rob_env_checker.update_robot_state(robot_link_poses);
  start = std::chrono::high_resolution_clock::now();

  for (int i=0;i<1500;++i)
  {
    // Get robot poses from manually:
    rob_env_checker.rob_env_ccheck_multi(robot_link_poses,c_data);
    
  }
  finish = std::chrono::high_resolution_clock::now();
  elapsed_secs=std::chrono::duration_cast<milli>(finish - start).count();
  cerr<<"Time taken for collision checking(multi): "<<elapsed_secs/ms<<endl;
  //rob_env_checker.get_link_markers(l_m);
  /*
  ros::Publisher m_pub=nh.advertise<visualization_msgs::MarkerArray>("link_collision_models", 1);
  while(ros::ok())
  {
    m_pub.publish(l_m);
  }
  */
}
