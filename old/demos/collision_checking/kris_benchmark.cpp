//#include <ll4ma_collision_wrapper/libs/kris_library/collision_checker.h>
#include <ll4ma_collision_wrapper/libs/kris_library/rob_env.h>
#include <array>
#include <fstream>
#include <iostream>
#include <ros/ros.h>
#include <ros/package.h>
#include <ctime>
#include <chrono>

using namespace kris_ccheck;
using namespace Meshing;
using namespace Geometry;

typedef double S;


int main(int argc, char* argv[])
{

  using milli = std::chrono::milliseconds;
  auto start = std::chrono::high_resolution_clock::now();
  auto finish = std::chrono::high_resolution_clock::now();
  double elapsed_secs=std::chrono::duration_cast<milli>(finish - start).count();
  double ms=1000.0;
  
  robEnv c_check(1,0.1);
  start = std::chrono::high_resolution_clock::now();
  // This should from your data directory:
  string pcd_file=ros::package::getPath("ll4ma_collision_wrapper");
  pcd_file.append("/data/table.pcd");
  ROS_INFO_STREAM("Loading pcd file from location " << pcd_file);
  

  

  
  // load pointcloud from file
  Eigen::Matrix4d t;
  t.setIdentity();
  CollisionMesh* env=new CollisionMesh();
  env=c_check.loadPCDFile(pcd_file,t);
  
  // create collision objects from octree boxes:
  
  finish = std::chrono::high_resolution_clock::now();
  cerr<<"Time to load PointCloud to collision model: "<<std::chrono::duration_cast<milli>(finish - start).count()/ms<<endl;
  // check with primitive shape:
  CollisionMesh* obj=new CollisionMesh();
  obj=c_check.loadBoxShape(0.1,0.1,0.1,t);


  vector<double> obj_position={0.0,0.0,0.0};

  //TODO:Broadphase collision checking
  //DynamicAABBTreeCollisionManager<S>* manager = new DynamicAABBTreeCollisionManager<S>();
  //manager->registerObjects(boxes);
  //manager->setup();
  //manager->octree_as_geometry_collide = true;
  //manager->octree_as_geometry_distance = true;

  start = std::chrono::high_resolution_clock::now();
  
  int i=0;
  Math3D::RigidTransform rt;
  while(i<100)
  {
    Math3D::Vector3 dir(obj_position[0],obj_position[1],obj_position[2]);
    rt.setTranslation(dir);
    obj->UpdateTransform(rt);


    // check collision between box and octomap env:
    coll_data res;
    c_check.signedDistance(obj,env,res,true);
    //c_check.checkCollision(obj,env,res);
    // add offset to position:
    obj_position[0]=0.0;
    obj_position[2]+=0.001;
    i++;
  }

  finish = std::chrono::high_resolution_clock::now();
  cerr<<"Time to compute signed distance(1000 calls): "<<std::chrono::duration_cast<milli>(finish - start).count()/ms<<endl;

  /*
  // read collision shapes from urdf:
  // read urdf from parameter server:
  ros::init(argc, argv, "fcl_demo");
  
  ros::NodeHandle nh;

  std::string robot_desc_string;
  nh.param("robot_description", robot_desc_string, std::string());
  vector<string> link_names={"lbr4_0_link", "lbr4_1_link", "lbr4_2_link", "lbr4_3_link", "lbr4_4_link", "lbr4_5_link", "lbr4_6_link"};
  vector<CollisionObjectd*> c_objs;
  vector<Transform3d> l_offsets;
  c_check.readLinkShapes(robot_desc_string,link_names,c_objs,l_offsets);
  */
  return 0;

}
