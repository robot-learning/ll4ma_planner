// This program visualizes the links of the kuka and changes color to indicate collisions.
// Allows for testing if the urdf is read correctly by the collision library and also if kdl offsets are correct.
#include "ll4ma_planner/problems/palm_pose_problem.hpp"
#include "ll4ma_planner/pagmo_opt/trajectory_optimization.hpp"

// Robot kdl:
#include <ros/ros.h>
#include <ros/package.h>

// ROS service:
#include "ll4ma_planner/IKJointTrajectory.h"
#include <ll4ma_collision_wrapper/UpdateEnvCloud.h>
// joint trajectory
#include <trajectory_msgs/JointTrajectory.h>
#include <sensor_msgs/JointState.h>
using namespace pagmo;

using namespace fcl_ccheck;

class srvClass
{
public:
  graspEnv c_check;
  ros::NodeHandle* nh;
  ros::Publisher pub,m_pub,l_pub;
  manipulator_kdl::robotKDL lbr4_,lbr4_allegro_;
  vector<vector<coll_data>> arm_c_data,hand_c_data;
  bool collision_ready=false;
  void init(ros::NodeHandle &n)
  {
    nh=&n;
    int n_links=8;
    float voxel_filter_size=0.02;
    //float hacd_vsize=0.1;
    vector<float> min_ws_bound={-0.8,-1.2,-0.1};
    vector<float> max_ws_bound={0.8,-0.4,1.2};
    
    //cerr<<"initializing rob_env_scene"<<endl;
    graspEnv rob_env_checker(n_links, voxel_filter_size,min_ws_bound,max_ws_bound);
    c_check=rob_env_checker;
    //cerr<<"copied rob_env_scene"<<endl;
  }
  void init_robot_meshes()
  {

    vector<string> link_names={"lbr4_0_link", "lbr4_1_link", "lbr4_2_link", "lbr4_3_link", "lbr4_4_link", "lbr4_5_link", "lbr4_6_link","palm_link"};

    vector<string> hand_link_names={"index_link_1","index_link_2","index_tip", "index_link_1","index_link_2","index_tip","index_link_1","index_link_2","index_tip", "thumb_link_2","thumb_link_3","thumb_tip"};

    l_pub=nh->advertise<visualization_msgs::MarkerArray>("link_env", 1);

    // initialize collision environment:
    std::string robot_desc_string;
    
    // read urdf as a string:
    nh->param("robot_description", robot_desc_string, std::string());
    c_check.load_robot_links(link_names,robot_desc_string);
    c_check.load_hand_links(hand_link_names,robot_desc_string);
    
      
    cerr<<"Created robot collision files"<<endl;
    
    vector<string> ee_names={"palm_link"};
    vector<string> base_names={"base_link"};
    vector<double> g_vec={0.0,0.0,-9.8};
    

    vector<string> hand_base_names={"base_link","base_link","base_link","base_link"};
    vector<string> hand_ee_names={"index_tip","middle_tip","ring_tip","thumb_tip"}; 

    manipulator_kdl::robotKDL lbr4("robot_description",*nh,base_names,ee_names,link_names,g_vec);

    manipulator_kdl::robotKDL lbr4_allegro("robot_description",*nh,hand_base_names, hand_ee_names, hand_link_names,g_vec); 
    //lbr4_.urdfParser();
    //lbr4_allegro_.urdfParser();
    lbr4_allegro_.dof=16;
    lbr4_.dof=7;

    lbr4_=lbr4;
    lbr4_allegro_=lbr4_allegro;
 
  }
  bool update_env_cloud(ll4ma_collision_wrapper::UpdateEnvCloud::Request &req,ll4ma_collision_wrapper::UpdateEnvCloud::Response &res)
  {
    pub=nh->advertise<pcl::PointCloud<PointXYZ>>("/collision_checker/env",1);
    m_pub=nh->advertise<visualization_msgs::MarkerArray>("collision_env", 1);
    ros::Rate l(100);
    cerr<<"Updating environment cloud"<<endl;
    //convert sensor_msgs to normal pointcloud
    PointCloud<PointXYZ>::Ptr e_cloud(new PointCloud<PointXYZ>);
    // Update env_cloud:
    pcl::fromROSMsg(req.env_cloud,*e_cloud);

    c_check.update_env(e_cloud,req.transform_mat);

    visualization_msgs::MarkerArray oc_map;
    //c_check.get_env_markers(oc_map,"base_link");
    
    // visualize pointcloud:
    e_cloud->header.frame_id="base_link";
    for(int i=0;i<100;++i)
    {
      pub.publish(e_cloud);
      // visualize octomap:
      m_pub.publish(oc_map);
      l.sleep();
    }
    collision_ready=true;
    return true;
  }
  void jCallback(sensor_msgs::JointState j_msg)
  {
    Eigen::VectorXd js,hand_js;
    js.resize(7);
    hand_js.resize(7+16);
    for(int i=0;i<7;++i)
    {
      js[i]=j_msg.position[i];
      hand_js[i]=j_msg.position[i];
    }

    for(int i=0;i<16;++i)
    {
      hand_js[i+7]=j_msg.position[i+7];
    }
    if(collision_ready)
    {
      check_arm_coll(js);
      check_hand_coll(hand_js);
    }
  }
  void check_arm_coll(Eigen::VectorXd js)
  {
    // get link poses:

    vector<vector<double>> r_poses;
    lbr4_.getLinkPoses(js,r_poses);

    // check for collisions:
    c_check.rob_env_ccheck(r_poses,arm_c_data);
    visualization_msgs::MarkerArray l;
    //c_check.get_link_markers(l);
    /*for(int i=0;i<10;++i)
    {
      l_pub.publish(l);
    }
    */
    if(arm_c_data.size()>0)
    {
      for(int i=0;i<arm_c_data.size();++i)
      {
	for(int j=0;j<arm_c_data[i].size();++j)
	{
	  if(arm_c_data[i][j].distance<0.0)
	  {
	    cerr<<"link in collision: "<<i<<" d: "<<arm_c_data[i][j].distance<<endl;
	  }
	}
      }
    }
  }
  void check_hand_coll(Eigen::VectorXd js)
  {
    // get link poses:

    vector<vector<double>> r_poses;
    r_poses.resize(4*3);
    Eigen::VectorXd hand_q(7+4);
    // get link poses:
    hand_q.head(7)=js.head(7);
    Eigen::VectorXd l_pose_;

    for(int i=0;i<4;++i)
    {
      for(int j=0;j<3;++j)
      {
        hand_q.tail(4)=js.segment(7+i*4,4);
        lbr4_allegro_.getLinkPose(i*3+j,hand_q,l_pose_);
        lbr4_allegro_.getSTDVector(l_pose_,r_poses[i*3+j]);
      }
    }
    // check for collisions:
    c_check.hand_env_ccheck(r_poses,hand_c_data);
    if(hand_c_data.size()>0)
    {
      for(int i=0;i<hand_c_data.size();++i)
      {
	for(int j=0;j<hand_c_data[i].size();++j)
	{
	  if(hand_c_data[i][j].distance<0.0)
	  {
	    cerr<<"hand link in collision: "<<i<<" d: "<<hand_c_data[i][j].distance<<endl;
	  }
	}
      }
    }


  }
  
};


int main(int argc, char** argv)
{

  //collisionChecker c_check;
  ros::init(argc,argv,"lbr4_ik_collision_node");
  ros::NodeHandle n;
  ros::Rate loop_rate(10);
  srvClass srv_call;
  srv_call.init(n);
  srv_call.init_robot_meshes();
  ros::ServiceServer env_service=n.advertiseService( "/ll4ma_planner/update_env", &srvClass::update_env_cloud, &srv_call);
  ros::Subscriber jsub=n.subscribe("/joint_states",1,&srvClass::jCallback,&srv_call);
  ros::spin();

}
