#include <ll4ma_collision_wrapper/scenes/rob_env.h>
#include <ros/ros.h>
#include <ros/package.h>
#include <ctime>
int main()
{
  using milli = std::chrono::milliseconds;
  auto start = std::chrono::high_resolution_clock::now();
  auto finish = std::chrono::high_resolution_clock::now();
  double elapsed_secs=std::chrono::duration_cast<milli>(finish - start).count();
  double ms=1000.0;
  //
  int DOF=8;
  string mesh_location=ros::package::getPath("urlg_robots_description");

  mesh_location.append("/meshes/lbr4/convex_hull/vertices/");

  // robot mesh file names:
  vector<string> r_files={"link_0.ply", "link_1.ply", "link_2.ply", "link_3.ply", "link_4.ply", "link_5.ply", "link_6.ply","link_7.ply"};

  for(int i=0;i<DOF;++i)
  {
    r_files[i].insert(0,mesh_location);
  }

  // This should from your data directory:
  string pcd_file=ros::package::getPath("ll4ma_collision_wrapper");
  pcd_file.append("/data/table.pcd");
  ROS_INFO_STREAM("Loading pcd file from location " << pcd_file);

  // initialize robEnv object:
  // variables:
  int n_links=8;
  float voxel_filter_size=0.01;
  float narrow_vsize=0.01;
  float broad_vsize=1.0;
  vector<float> min_ws_bound={-1.2,-1.2,0.1};
  vector<float> max_ws_bound={1.2,1.2,1.2};


  robEnv rob_env_checker(n_links, voxel_filter_size,narrow_vsize,broad_vsize,min_ws_bound,max_ws_bound);


  // load pointcloud from file:

  PointCloud<PointXYZ>::Ptr env_cloud(new PointCloud<PointXYZ>);
  start = std::chrono::high_resolution_clock::now();
  rob_env_checker._pcl_eigen.loadPCD(pcd_file,env_cloud);
  finish = std::chrono::high_resolution_clock::now();
  elapsed_secs=std::chrono::duration_cast<milli>(finish - start).count();

  cerr<<"Loading pcd took:"<<elapsed_secs/ms<<endl;

  // initialize environment:
  geometry_msgs::Transform base_frame;
  base_frame.translation.x=0.0;
  base_frame.translation.y=0.0;
  base_frame.translation.z=0.0;
  base_frame.rotation.x=0.0;
  base_frame.rotation.y=0.0;
  base_frame.rotation.z=0.0;
  base_frame.rotation.w=1.0;
  start = std::chrono::high_resolution_clock::now();

  rob_env_checker.update_env(env_cloud,base_frame);
  finish = std::chrono::high_resolution_clock::now();
  elapsed_secs=std::chrono::duration_cast<milli>(finish - start).count();

  cerr<<"Loading env took:"<<elapsed_secs/ms<<endl;

  start = std::chrono::high_resolution_clock::now();

  // load robot meshes:
  rob_env_checker.load_robot_links(r_files);
  finish = std::chrono::high_resolution_clock::now();
  elapsed_secs=std::chrono::duration_cast<milli>(finish - start).count();
  cerr<<"Loading robot meshes took:"<<elapsed_secs/ms<<endl;
  vector<vector<double>> robot_link_poses;
  robot_link_poses.emplace_back(vector<double>{1.0,0.0,0.0,0.0,0.0,0.0,1.0});
  robot_link_poses.emplace_back(vector<double>{1.0,0.0,0.11,0.0,0.0,0.0,1.0});
  robot_link_poses.emplace_back(vector<double>{1.0,0.0,0.31,0.0,0.0,0.0,1.0});
  robot_link_poses.emplace_back(vector<double>{1.0,0.0,0.502,0.0,0.0,0.0,1.0});
  robot_link_poses.emplace_back(vector<double>{1.0,0.0,0.702,0.0,0.0,0.0,1.0});
  robot_link_poses.emplace_back(vector<double>{1.0,0.0,0.902,0.0,0.0,0.0,1.0});
  robot_link_poses.emplace_back(vector<double>{1.0,0.0,1.092,0.0,0.0,0.0,1.0});
  robot_link_poses.emplace_back(vector<double>{1.0,0.0,1.17,0.0,0.0,0.0,1.0});

  vector<vector<collision_data>> c_data;

  start = std::chrono::high_resolution_clock::now();

  for (int i=0;i<500;++i)
  {
    // Get robot poses from manually:
    rob_env_checker.rob_env_ccheck(robot_link_poses,c_data);

  }
  finish = std::chrono::high_resolution_clock::now();
  elapsed_secs=std::chrono::duration_cast<milli>(finish - start).count();
  cerr<<"Time taken for collision checking: "<<elapsed_secs/ms<<endl;
}
