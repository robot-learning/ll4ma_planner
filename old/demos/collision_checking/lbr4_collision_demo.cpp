// This program visualizes the links of the kuka and changes color to indicate collisions.
// Allows for testing if the urdf is read correctly by the collision library and also if kdl offsets are correct.
#include "ll4ma_planner/problems/collision_opt_problem.hpp"
#include "ll4ma_planner/pagmo_opt/trajectory_optimization.hpp"

// Robot kdl:
#include <ros/ros.h>
#include <ros/package.h>

// ROS service:
#include "ll4ma_planner/IKJointTrajectory.h"
#include <ll4ma_collision_wrapper/UpdateEnvCloud.h>
// joint trajectory
#include <trajectory_msgs/JointTrajectory.h>
#include <sensor_msgs/JointState.h>
using namespace pagmo;

using namespace kris_ccheck;

class srvClass
{
public:
  robEnv c_check;
  ros::NodeHandle* nh;
  ros::Publisher pub,m_pub,l_pub;
  manipulator_kdl::robotKDL lbr4_;//("robot_description",*nh,base_names,ee_names,link_names,g_vec);
  vector<coll_data> c_data;
  void init(ros::NodeHandle &n)
  {
    nh=&n;
    int n_links=8;
    float voxel_filter_size=0.02;
    //float hacd_vsize=0.1;
    //vector<float> min_ws_bound={-0.8,-1.2,-0.1};
    //vector<float> max_ws_bound={0.8,-0.4,1.2};
    vector<float> min_ws_bound={-1.2,-1.2,0.1};
    vector<float> max_ws_bound={1.2,1.2,1.2};

    //cerr<<"initializing rob_env_scene"<<endl;
    robEnv rob_env_checker(n_links, voxel_filter_size,min_ws_bound,max_ws_bound);
    c_check=rob_env_checker;
    //cerr<<"copied rob_env_scene"<<endl;
  }
  void init_robot_meshes()
  {

    vector<string> link_names={"lbr4_0_link", "lbr4_1_link", "lbr4_2_link", "lbr4_3_link", "lbr4_4_link", "lbr4_5_link", "lbr4_6_link","lbr4_7_link"};
    l_pub=nh->advertise<visualization_msgs::MarkerArray>("contact_points", 1);

    // initialize collision environment:
    std::string robot_desc_string;
    
    // read urdf as a string:
    nh->param("robot_description", robot_desc_string, std::string());
    c_check.load_robot_links(link_names,robot_desc_string);
    
      
    cerr<<"Created robot collision files"<<endl;

    vector<string> ee_names={"lbr4_7_link"};
    vector<string> base_names={"base_link"};
    vector<double> g_vec={0.0,0.0,-9.8};
    

    manipulator_kdl::robotKDL lbr4("robot_description",*nh,base_names,ee_names,link_names,g_vec);

    lbr4_=lbr4;
    cerr<<"Created kdl object"<<endl;

    // load env:

    /*
    string pcd_file=ros::package::getPath("ll4ma_collision_wrapper");
    pcd_file.append("/data/table.pcd");
    ROS_INFO_STREAM("Loading pcd file from location " << pcd_file);




    // load pointcloud from file:
    
    pcl::PointCloud<pcl::PointXYZ>::Ptr env_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    c_check._pcl_eigen.loadPCD(pcd_file,env_cloud);
    geometry_msgs::Transform base_frame;
    base_frame.translation.x=0.0;
    base_frame.translation.y=0.0;
    base_frame.translation.z=0.0;
    base_frame.rotation.x=0.0;
    base_frame.rotation.y=0.0;
    base_frame.rotation.z=0.0;
    base_frame.rotation.w=1.0;

    c_check.update_env(env_cloud,base_frame);
    pub=nh->advertise<pcl::PointCloud<pcl::PointXYZ>>("/collision_checker/env",1);
    env_cloud->header.frame_id="base_link";
    ros::Rate l(100);

    for(int i=0;i<100;++i)
    {
      pub.publish(env_cloud);
      // visualize octomap:
      //m_pub.publish(oc_map);
      l.sleep();
    }
    */

    // load mesh as environment
    m_pub=nh->advertise<visualization_msgs::Marker>("collision_env", 1);
    string stl_file=ros::package::getPath("ll4ma_collision_wrapper");
    stl_file.append("/data/table_shelf.stl");

    visualization_msgs::Marker marker;
    marker.header.frame_id = "base_link";
    marker.ns = "basic_shapes";
    marker.id = 0;
  
    // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
  
    // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
    marker.action = visualization_msgs::Marker::ADD;
    
    // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    marker.pose.position.x = 0.75;
    marker.pose.position.y = 0.45;
    marker.pose.position.z = -0.615;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 1.0;
    marker.pose.orientation.w = 0.0;
  
    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker.scale.x = 1.0;
    marker.scale.y = 1;
    marker.scale.z = 1;

    // Set the color -- be sure to set alpha to something non-zero!
    marker.color.r = 0.8f;
    marker.color.g = 0.8f;
    marker.color.b = 0.8f;
    marker.color.a = 1.0;

    marker.lifetime = ros::Duration();
    

  // mesh marker:
    uint32_t mesh_shape= visualization_msgs::Marker::MESH_RESOURCE;
    marker.type=mesh_shape;
    marker.mesh_resource="file:///home/bala/catkin_ws/src/ll4ma_collision_wrapper/data/table_shelf2.stl";

    

    // load pointcloud from file:

    vector<string> f_names={stl_file};
    vector<Eigen::Matrix4d> t_;
    t_.resize(1);
    t_[0].setIdentity();
    t_[0](0,3)=0.75;
    t_[0](1,3)=0.45;
    t_[0](2,3)=-0.615;
    t_[0](0,0)=-1;
    t_[0](1,1)=-1;
    
    c_check.add_env_meshes(f_names,t_);


    // create env marker:
    
    //env_cloud->header.frame_id="base_link";
    ros::Rate l(100);

    for(int i=0;i<100;++i)
    {
      // visualize octomap:
      m_pub.publish(marker);
      l.sleep();
    }
  }
  bool update_env_cloud(ll4ma_collision_wrapper::UpdateEnvCloud::Request &req,ll4ma_collision_wrapper::UpdateEnvCloud::Response &res)
  {
    pub=nh->advertise<pcl::PointCloud<pcl::PointXYZ>>("/collision_checker/env",1);
    //m_pub=nh->advertise<visualization_msgs::MarkerArray>("collision_env", 1);
    cerr<<"Updating environment cloud"<<endl;
    //convert sensor_msgs to normal pointcloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr e_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    // Update env_cloud:
    pcl::fromROSMsg(req.env_cloud,*e_cloud);

    c_check.update_env(e_cloud,req.transform_mat);

    //visualization_msgs::MarkerArray oc_map;
    //c_check.get_env_markers(oc_map,"base_link");
    
    // visualize pointcloud:
    e_cloud->header.frame_id="base_link";
    for(int i=0;i<100;++i)
    {
      pub.publish(e_cloud);
      // visualize octomap:
      //m_pub.publish(oc_map);
      //l.sleep();
    }

    return true;
  }
  void jCallback(sensor_msgs::JointState j_msg)
  {
    Eigen::VectorXd js;
    js.resize(j_msg.position.size());
    for(int i=0;i<js.size();++i)
    {
      js[i]=j_msg.position[i];
    }
    check_coll(js);
  }
  void check_coll(Eigen::VectorXd js)
  {
    // get link poses:

    vector<vector<double>> r_poses;
    //cerr<<"getting link poses"<<endl;
    lbr4_.getLinkPoses(js,r_poses);
    //cerr<<"got link poses"<<r_poses.size()<<endl;
    // check for collisions:
    c_check.rob_env_ccheck(r_poses,c_data);
    //cerr<<"got collision data "<<endl;
    
    visualization_msgs::MarkerArray l_p;
    
    // reset markerarray:
    uint32_t shape = visualization_msgs::Marker::SPHERE;
    // create markers:
      visualization_msgs::Marker marker;
      marker.header.frame_id = "/base_link";
      marker.ns = "basic_shapes";
      marker.id = 0;
  
      // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
      marker.type = shape;
      
      // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
      marker.action = visualization_msgs::Marker::ADD;
  
      // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
      marker.pose.orientation.x = 0.0;
      marker.pose.orientation.y = 0.0;
      marker.pose.orientation.z = 0.0;
      marker.pose.orientation.w = 1.0;
      
      // Set the scale of the marker -- 1x1x1 here means 1m on a side
      marker.scale.x = 0.01;
      marker.scale.y = 0.01;
      marker.scale.z = 0.01;
      
      // Set the color -- be sure to set alpha to something non-zero!
      marker.color.r = 1.0f;
      marker.color.g = 0.0f;
      marker.color.b = 0.0f;
      marker.color.a = 1.0;
  
      marker.lifetime = ros::Duration();
      
      if(c_data.size()>0)
      {
        for(int i=0;i<c_data.size();++i)
        {
          if(c_data[i].distance<0.0)
          {
            // add to marker
            cerr<<"link in collision: "<<i<<" d: "<<c_data[i].distance<<endl;
            marker.id+=1;
            marker.pose.position.x = c_data[i].p1[0];
            marker.pose.position.y = c_data[i].p1[1];
            marker.pose.position.z = c_data[i].p1[2];

            l_p.markers.emplace_back(marker);
            
          }
        }
      }
      
    // publish markerarray:
    l_pub.publish(l_p);
  }
  
};


int main(int argc, char** argv)
{

  //collisionChecker c_check;
  ros::init(argc,argv,"lbr4_ik_collision_node");
  ros::NodeHandle n;
  ros::Rate loop_rate(10);
  srvClass srv_call;
  srv_call.init(n);
  srv_call.init_robot_meshes();
  cerr<<"Initialized robot meshes"<<endl;
  // load environment:
  
  ros::ServiceServer env_service=n.advertiseService( "/ll4ma_planner/update_env", &srvClass::update_env_cloud, &srv_call);

  ros::Subscriber jsub=n.subscribe("/joint_states",1,&srvClass::jCallback,&srv_call);
  ros::spin();
  /*
  while(ros::ok())
  {
    //
    loop_rate.sleep();
  }
  */
}
